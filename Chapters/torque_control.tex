\chapter{Joint torque control and external force estimation} \label{sec:torque_control}

As previously mentioned, the robot dynamic model is often required for safe pHRI, since its knowledge allows:
\begin{itemize}
	\item
	low level torque control,
	\item
	estimation of the external (often, human-applied) forces and torques $\ForcesExt \in \RealNumbers^6$,
	\item
	derivation of interesting metrics, e.g., the reflected inertia\cite{Khatib:95}, explained in \RefSec{sec:kinetic_energy_limitation}.
\end{itemize}

However, the robot dynamic model is generally not provided by the manufacturers\footnote{Some may provide approximate parameters based on CAD data.} and therefore an identification procedure is required.
Furthermore, any modification to the robot, such as mounting a tool at the end-effector, requires updating the model.
Collaborative robots may be subject to frequent tool exchange if the operators require different tools to perform different tasks using the same robot.
In such cases, the robot must be able to quickly identify its tool to update its dynamic model in order to gain accuracy in its positioning and in the estimation of the interaction forces and torques.

To this end, we developed a novel approach for identifying the inertial parameters (links' mass, center of mass, inertia matrix) and joint friction coefficients (viscous and dry friction) of a Kuka LWR4+ using an optimal exciting motion\footnote{This joint work with Katsumata et al. has been submitted to Robotics and Autonomous Systems.}. %in\cite{Katsumata:17}.
In that work, we compared three different cost functions to maximize the identification accuracy of the identified parameters, while minimizing the exciting motion duration.
We also considered the geometric model identification of a tool based on a look-up table and on an inverse geometric model of the robot.

Since the modeling and estimation part of this work has been mainly developed by the co-authors, %of\cite{Katsumata:17}
in this thesis we will focus on the estimation of the external forces/torques and on torque control, which are the aspects I contributed to.
In particular, we show the difficulties that arise when dealing with static friction for torque control.
We propose a solution to this, and validate it in experiments, including one where the robot collides with a human operator, a case study of major interest in safe pHRI.

\section{External torques and forces estimation} \label{sec:external_torques}
In order to detect physical interaction with the environment or the operator intention, two approaches are generally considered.

The first one consists in mounting a force/torque sensor at the robot end-effector to measure interaction forces and torques.
If a tool is attached to the sensor, its inertial parameters must be identified in order to remove its effects on the measurements.
Physical interaction with a human usually happen with a slowly moving robot, so the identification of the tool mass and center of mass is sufficient to get a good compensation.
This translates to:
\begin{align}
\ForcesExt = \Forces_{FT} - \begin{bmatrix}
m_{tool} \RotationMatrix{T}{B} ~ \Bold{g}	\\
\Bold{c}_{tool} \times m_{tool} \RotationMatrix{T}{B} ~ \Bold{g}
\end{bmatrix},
\end{align}
where $\Forces_{FT} \in \RealNumbers^6$ is the force/torque vector measured by the sensor, $m_{tool}$ and $\Bold{c}_{tool}$ are the mass and center of mass of the tool, given in the sensor frame, $\Bold{g}$ is the Earth gravity vector expressed in the robot base frame and $\RotationMatrix{T}{B}$ is the rotation matrix between the robot base and tool frames.

The second approach is applicable when joint torque sensors are mounted between the gearbox output and the attached link.
In such a case, as it is with the Kuka LWR4+, the knowledge of the robot dynamic model allows for individual joint external torque estimation and for reconstructing the external forces/torques at the end-effector, using the Jacobian matrix.
Let us first recall the dynamic model expression for a rigid serial manipulator in the free space (i.e., when no external forces/torques are applied):
\begin{align}
\InertiaMatrix\JointsAcc + \CoriolisCentripetal\JointsVel + \GravityVector + \Torques_f(\JointsVel) &= \Torques_{dyn}, \label{eq:dynamic_model}
\end{align}
with $\InertiaMatrix \in \RealNumbers^{n \times n}$ the robot inertia matrix, $\CoriolisCentripetal \in \RealNumbers^{n \times n}$ the Coriolis and centripetal matrix, $\GravityVector \in \RealNumbers^{n}$ the vector of gravity torques and $\Torques_f \in \RealNumbers^{n}$ the joint torques due to dry and viscous friction effects.
$n$ is the joint space dimension.
In the presence of external interaction torques $\TorquesExt$ or forces $\ForcesExt$:
\begin{align}
\Torques &= \Torques_{dyn} + \TorquesExt \label{eq:dynamic_model_ext_torques} \\
\Torques &= \Torques_{dyn} + \Trans{\Jacobian}(\Joints)\ForcesExt,
\label{eq:dynamic_model_ext_forces}
\end{align}
where $\Torques \in \RealNumbers^{n}$ indicates the torques acting on the joints and $\Jacobian  \in \RealNumbers^{6 \times n}$ the manipulator Jacobian matrix tied to the point of application of $\ForcesExt$, generally assumed to be the end-effector. In \RefEq{eq:dynamic_model_ext_torques}, $\TorquesExt \in \RealNumbers^{n}$ is the vector of externally applied torques.
In \RefEq{eq:dynamic_model_ext_forces}, $\ForcesExt \in \RealNumbers^{6}$ is the external wrench applied at the end-effector, mapped to the joint space using the manipulator's Jacobian matrix. Using~\RefEq{eq:dynamic_model}, the above equations become:
\begin{align}
\Torques &= \InertiaMatrix\JointsAcc + \CoriolisCentripetal\JointsVel + \GravityVector + \Torques_f(\JointsVel) + \TorquesExt \label{eq:dynamic_model_ext_torques2} \\
\Torques &= \InertiaMatrix\JointsAcc + \CoriolisCentripetal\JointsVel + \GravityVector + \Torques_f(\JointsVel) + \Trans{\Jacobian}(\Joints)\ForcesExt.
 \label{eq:dynamic_model_ext_forces2}
\end{align}
With an accurate dynamic model and joint torque sensors, the external joint torques and external wrench, both needed to detect interactions at the end-effector or along the kinematic chain, can be computed using:
\begin{align}
\TorquesExt &= \Torques - \Torques_{dyn} \label{eq:external_torques_estimation} \\
\ForcesExt &= \Jacobian^{-\top}(\Joints)\TorquesExt \label{eq:external_forces_estimation}
\end{align}
In the case of a redundant manipulator, the Moore-Penrose pseudo-inverse can be used in \RefEq{eq:external_forces_estimation} instead of the classic matrix inversion.
In the case of the Kuka LWR4+, both $\TorquesExt$ and $\ForcesExt$ can be computed thanks to the presence of joint torque sensors.\\


To conclude on this section, it is important to note that the two presented approaches are not perfect.
Indeed, using a force/torque sensor at the end-effector often requires integration work for attaching the device to the robot, dealing with the sensor cable, etc.
Moreover, it cannot be used to detect collisions or interactions forces on points along the robot other than the end-effector, leading to potential security issues.
On the other hand, as mentioned previously, joint torque sensors are not always available due to their cost and to the increased integration complexity. Besides, even when available, they cannot be used to estimate the external wrench at the end-effector in the presence of kinematic singularities, as can be seen from \RefEq{eq:external_forces_estimation}, nor the location and magnitude of a contact along the kinematic chain. The same applies to the first approach, unless a tactile skin covers the robot joints, providing the contact point position.
For the best sensing capabilities, both solutions should be used jointly but this, of course, comes with an increased cost and system complexity.
In our case, we prefer to rely on a force/torque sensor at the end-effector for a more precise estimation of the interaction forces and to use external joint torques only for collision detection.
A precise end-effector wrench estimation allows for better hand guiding or force control for instance while even an approximate estimation of the external joint torques is sufficient to detect unexpected collisions along the robot's body that might occur during pHRI and then take a preventive action (e.g. a \textit{safety-rated monitored stop}).


\section{Torque control of a Kuka LWR4+}
As seen in the previous section, to estimate the interaction forces using joint torque sensors, the knowledge of the joints torque commands is necessary.
Since this data is not generally offered by the robots embedded controllers, torque control must be performed on the user side. In this section, we will detail how torque control can be achieved on a Kuka LWR4+.

The Kuka LWR4+ controller (KRC) is shipped with three different control modes:
\begin{itemize}
		\item joint position control,
		\item joint impedance control,
		\item cartesian impedance control.
\end{itemize}
It can be seen that no torque control mode is directly available.
This means that workarounds should be implemented on both the KRC side and the PC control interface library FRI\footnote{FRI (Fast Research Interface), available at \url{http://cs.stanford.edu/people/tkr/fri/html/}}, to control the robot actuator torques.
Among the three available control modes, joint impedance control is the only one that can be adapted to perform joint torque control.
First, let us recall the joint impedance control equation, given in the KRC manual and FRI documentation, and rewritten for clarity as:
\begin{align}
\Command\Torques = \Bold{K}_j\Delta\Joints + \Bold{D}(\Bold{d}_j) + \Torques_{dynamics}(\Joints, \JointsVel, \JointsAcc, \Bold{g}) + \Torques_{FRI}. \label{eq:fri_joint_impedance}
\end{align}
In this equation, $\Command\Torques \in \RealNumbers^7$ are the command torques sent to the actuators, $\Bold{K}_j$ is a 7x7 diagonal matrix of stiffness parameters, $\Bold{D} \in \RealNumbers^7$ is a vector relative damping torques\footnote{The actual damping torques are computed relative to the stiffness coefficients.} parametrized by $\Bold{d}_j \in \Interval{0}{1}^7$, $\Delta\Joints = \Reference\Joints - \Joints \in \RealNumbers^7$ is the tracking error, $\Torques_{dynamics}$ is the embedded dynamic model\footnote{This is not detailed in the documentation, but probably corresponds only to gravity compensation.} and $\Torques_{FRI}$ is an additional input torque that can be set through FRI.
In usual, $\Torques_{FRI}$ is set to zero in order to meet the expected joint impedance behavior but it is thanks to this variable that we can achieve torque control.

It is clear from \RefEq{eq:fri_joint_impedance} that to achieve torque control, all the terms on the right hand side of the equation, except for $\Torques_{FRI}$, must be canceled.
This is done by setting $\Bold{K}_j = \Bold{0}$, $\Bold{d}_j = \Bold{0}$ $\forall j = \left\{ 1, \dots, 7 \right\}$ and $\Bold{g} = \Bold{0}$, where $\Bold{g}$ is Earth's gravity vector configured in the KRC and used by $\Torques_{dynamics}$.
Doing so leaves us with:
\begin{align}
	\Command\Torques = \Torques_{FRI}. \label{eq:fri_torque_control}
\end{align}
Equation \RefEq{eq:fri_torque_control} can then be used to send the desired torque commands directly to the robot actuators.
Modifications of the FRI library and KRC scripts to enable torque control are available online\footnote{\url{https://github.com/BenjaminNavarro/api-driver-fri}}.

However, it is also important to note that the torque tracking accuracy on this robot is far from perfect, as can be seen from our tests, that are presented in figures \ref{fig:torque_tracking_error} and \ref{fig:torque_tracking}.
The first figure gives the average error and standard deviation for each joint torque.
The second one displays the difference between the torques sent to the robot ($\Command\Torques$) and the ones measured by the torque sensors ($\Torques$). The data in Fig.~\ref{fig:torque_tracking} has been prefiltered using a low pass 2nd order Butterworth filter with 10Hz cutoff frequency, to improve readability.
Since Kuka provides no details about the KRC controller, there is no way to pin-point the exact cause of these errors and they are most probably tied to force sensor inaccuracies, unmodeled and/or uncompensated joint frictions and/or to the robot's torque regulation loop.
Even if these errors may seem reasonable ($<1$ N.m), they are problematic for motions with low accelerations producing low torque variations, as we will show in \RefSec{sec:torque_control_scheme}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.9\textwidth]{torque_control/torque_tracking_error.eps}
	\caption{Average torque tracking error and standard deviation on a Kuka LWR4+.}
	\label{fig:torque_tracking_error}
\end{figure}

\begin{figure}[hb!]
	\centering
	\includegraphics[width=0.9\textwidth]{torque_control/torque_tracking.eps}
	\caption{Torque tracking accuracy on a Kuka LWR4+.}
	\label{fig:torque_tracking}
\end{figure}




\section{Torque control scheme} \label{sec:torque_control_scheme}
In this section, we detail the torque control scheme used to drive the Kuka LWR4+ to a target joint configuration.

A classic approach to realize joint positioning using torque control is the computed torque method\cite{HandbookRobotics:08}:
\begin{align}
\Command\Torques = \InertiaMatrix(\Reference\JointsAcc + \Bold{K}_p \Delta\Joints + \Bold{K}_v \Delta\JointsVel) + \CoriolisCentripetal\JointsVel + \GravityVector + \Torques_f(\JointsVel). \label{eq:computed_torque_control}
\end{align}
In this equation, $\Bold{K}_p$ and $\Bold{K}_v$ are positive diagonal gain matrices used to apply position and velocity feedback in addition to the feedforward acceleration term $\Reference\JointsAcc$.
Vector $\Torques_f$ is generally expressed as:
\begin{align}
\FrictionTorques(\JointsVel) &= \ViscousFrictionCoeff \JointsVel + \DryFrictionCoeff \sign(\JointsVel), \label{eq:dry_viscous_friction}
\end{align}
where $\ViscousFrictionCoeff$ and $\DryFrictionCoeff$ are the diagonal matrices of viscous and static friction coefficients, respectively.
These coefficients must be identified on a robot basis.

However, using \RefEq{eq:dry_viscous_friction} is not a perfect real-world solution since stiction (static friction, torques are not constant across the robot configuration space.
To demonstrate this, we set-up an experiment with a Kuka LWR4+ mounted horizontally with the joint axes laying in the horizontal plane so that the gravity does not interfere, leading to all joints being at their zero position.
Then each joint is sequentially driven to five different positions (-2, -1, 0, 1 and 2 radians) before being brought back to zero using simple gravity compensation with PD control law:
\begin{equation}
\Command\Torques = \Bold{K}_p \Delta\Joints + \Bold{K}_v \Delta\JointsVel + \GravityVector
\end{equation}
At each position, the PD gains for the \textit{test joint} are zeroed and the desired torque value is slowly increased, starting from 0 with 0.01 N.m steps, until a motion is detected. This way, the minimum positive torque needed to overcome stiction is estimated. The same is performed with reversed sign, to measure the minimum negative torque that overcomes stiction.
To assess if, at a given position, the stiction torques are constant or not, the experiment was run three times.
The results are given in figures \ref{fig:average_stiction_torque} and \ref{fig:stiction_torques}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.8\textwidth]{torque_control/average_stiction_torques.eps}
	\caption{Average stiction torques and standard deviation of a Kuka LWR4+.}
	\label{fig:average_stiction_torque}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.7\textwidth]{torque_control/stiction_torques.eps}
	\caption{Stiction torques for each joint of a Kuka LWR4+.}
	\label{fig:stiction_torques}
\end{figure}

Several important aspects can be derived from these graphs.
First, for a given joint, the stiction torque is not constant. This forbids the use of constant values in $\DryFrictionCoeff$ as it would be insufficient in some cases or lead to overcompensation and thus instability in the others.
Secondly, for a given joint at a given position, the stiction torques are generally not symmetric. This avoids the use of the $\sign$ function in \RefEq{eq:dry_viscous_friction}. Finally, important variations can be measured between experiments, as can be seen for example for joint 2 at -2 radians in figure \ref{fig:stiction_torques}.
In summary, it is clear that the stiction torques cannot be compensated using \RefEq{eq:dry_viscous_friction} and that another solution must be considered.

In this regard, a first improvement over \RefEq{eq:computed_torque_control} would be to apply:
\begin{align}
\Command\Torques = \InertiaMatrix\Reference\JointsAcc + \Bold{K}_p \Delta\Joints + \Bold{K}_v \Delta\JointsVel + \CoriolisCentripetal\JointsVel + \GravityVector + \ViscousFrictionCoeff \JointsVel. \label{eq:computed_torque_control_modified}
\end{align}
With \RefEq{eq:computed_torque_control_modified}, the stiction is compensated using only the feedback terms ($\DryFrictionCoeff = \textbf{0}$) and without pre-multiplying them by the inertia matrix, thus producing always the same torque for a given tracking error.
However, this may not be satisfactory in all cases.
Indeed, to quickly compensate the deviations from the trajectory due to joint stiction, very high gains are required.
This leads to a very stiff position control scheme.
The solution we propose here is to decouple the stiction compensation problem from the more general perturbation rejection problem.
The main idea is to use two PD control laws: a first one with very high gains and limited torque output to overcome stiction and a second one that can be tuned freely to obtain the desired perturbation rejection behavior.

For the friction compensation part, this translates to:
\begin{align}
\FrictionTorques'(\JointsVel, \Delta\Joints) = \ViscousFrictionCoeff \JointsVel + _{-\Torques^c_{max}}\lfloor \Bold{K}^c_p \Delta\Joints + \Bold{K}^c_v \Delta\JointsVel \rceil^{\Torques^c_{max}}. \label{eq:dry_friction_compensation}
\end{align}
Now, the static friction is compensated using a PD controller with gains $\Bold{K}^c_p$ and $\Bold{K}^c_v$ and output limited to the $\Interval{-\Torques^c_{max}}{\Torques^c_{max}}$ range.
This presents the advantage of not relying on any stiction parameters estimation and cannot lead to overcompensation.

We then propose two alternative control schemes, based on equations \RefEq{eq:computed_torque_control}, \RefEq{eq:computed_torque_control_modified} and \RefEq{eq:dry_friction_compensation}:
\begin{align}
\Command\Torques &= \InertiaMatrix\Reference\JointsAcc + \Bold{K}_p \Delta\Joints + \Bold{K}_v \Delta\JointsVel  + \CoriolisCentripetal\JointsVel + \GravityVector + \FrictionTorques'(\JointsVel, \Delta\Joints) \label{eq:computed_torque_control_friction_torque_PD} \\
\Command\Torques &= \InertiaMatrix(\Reference\JointsAcc + \Bold{K}_p \Delta\Joints + \Bold{K}_v \Delta\JointsVel) + \CoriolisCentripetal\JointsVel + \GravityVector + \FrictionTorques'(\JointsVel, \Delta\Joints). \label{eq:computed_torque_control_friction_acc_PD}
\end{align}
The choice between \RefEq{eq:computed_torque_control_friction_torque_PD} and \RefEq{eq:computed_torque_control_friction_acc_PD} depends on the desired perturbation rejection behavior.
With the first equation, an opposing torque will be created when a tracking error appears.
This enables the limitation of the torque transmitted to the environment by choosing safe fixed values for $\Bold{K}_p$ and $\Bold{K}_v$ or even tuning their values online to keep the exerted torque below a given limit.
However, if a consistent perturbation rejection behavior is desired, the second control law is recommended, since the feedback action is passed through the inertia matrix and will produce the same motion, regardless of the current robot configuration.
These two control schemes are depicted in \RefFig{fig:torque_control_scheme}, where the choice between one and the other is made through the $S_{pos}$ switch.
This switch should not be toggled online as this can lead to discontinuities in the control output.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\textwidth]{torque_control/torque_control_scheme.eps}
	\caption{Proposed torque control scheme with stiction compensation.}
	\label{fig:torque_control_scheme}
\end{figure}

\section{Experiments}
To demonstrate the effectiveness of the proposed solution, a trajectory following experiment has been conducted using \RefEq{eq:computed_torque_control_friction_torque_PD} in the following scenarios:

\hspace{0.05\textwidth}
\begin{minipage}{0.85\textwidth}
\begin{enumerate}[label=Case \arabic*:]
	\item All feedback gains set to zero ($\Bold{K}_p = \Bold{K}_v = \Bold{K}^c_p = \Bold{K}^c_v = \Bold{0}$).
	\item Static PD friction compensation only ($\Bold{K}_p = \Bold{K}_v = \Bold{0}, \Bold{K}^c_p \neq \Bold{0}, \Bold{K}^c_v \neq \Bold{0}$).
	\item Static PD friction compensation only with a collision (human operator, $\Bold{K}_p = \Bold{K}_v = \Bold{0}$, $\Bold{K}^c_p \neq \Bold{0}, \Bold{K}^c_v \neq \Bold{0}$).
	\item Static PD friction compensation and PD control with a collision (human operator, $\Bold{K}_p \neq \Bold{0}$, $\Bold{K}_v \neq \Bold{0}$, $\Bold{K}^c_p \neq \Bold{0}$, $\Bold{K}^c_v \neq \Bold{0}$). \\
\end{enumerate}
\end{minipage}

\RefEq{eq:computed_torque_control_friction_torque_PD} has been chosen over \RefEq{eq:computed_torque_control_friction_acc_PD} in this case to have a non-configuration dependent reaction to externally applied torques.
The generated trajectory drives all the joints from $-1$ to $1$ radian with a maximum acceleration of $0.5 $ rad.s$^{-2}$ and a maximum velocity of $0.5 $ rad.s$^{-1}$. The values of $\InertiaMatrix$, $\CoriolisCentripetal$, $\GravityVector$ and $\ViscousFrictionCoeff$ in \RefEq{eq:computed_torque_control_friction_torque_PD} were obtained by dynamic model identification. The friction compensation parameters, when used, are $\Bold{K}^c_p = \Diag(10000, 5000, 2000, 2000, 500, 200, 200)$, $\Bold{K}^c_v = \Diag(50, 100, 30, 30, 20, 10, 10)$ and $\Torques^c_{max} = [3, 4, 2, 2, 2, 1, 1]$.
These parameters were manually tuned to try to get the best possible compensation. The numbering of the Kuka LWR4+ joints is given in Fig.~\ref{fig:joints_kuka}.

Results from the first test (case 1) are shown in figures \ref{fig:feedforward_only_error} and \ref{fig:feedforward_only_torques}.
In \RefFig{fig:feedforward_only_torques}, gravity torques as well as torques generated from the other feedforward terms are displayed separately.
It can be seen that the torques computed to perform the motion (i.e. excluding gravity compensation) are low and, considering the problems mentioned previously, are not sufficient to drive the joints to the target positions.
This is confirmed by \RefFig{fig:feedforward_only_error}, where the target and actual position of each joint is plotted.
Indeed, except for joint 2 where the braking torque makes the joint move in the wrong direction towards the end of the trajectory, all joints stay stationary.
This clearly demonstrates the need for a stiction compensation mechanism.

Figures \ref{fig:feedforward_with_fc_torques} and \ref{fig:feedforward_with_fc_error} give the results for the second experiment (case 2).
We notice from \RefFig{fig:feedforward_with_fc_torques} the presence of the friction compensation torques.
Thanks to these additional torques, the trajectory can be followed way more precisely than in the previous case, as can be seen in \RefFig{fig:feedforward_with_fc_error}.
We can see that at the beginning of the trajectories, the errors start to grow but are kept low thanks to the friction compensation mechanism described in \RefEq{eq:dry_friction_compensation}.
In this particular case, the errors generally stay below 0.005 radians (0.3 degrees), except for joint 2 where the error goes up to 0.008 radians (0.45 degrees) before being brought back to 0.

The next experiment (case 3) uses the same configuration as the previous one but includes a collision with a human operator at the end-effector.
Results are given in figures \ref{fig:feedforward_with_fc_with_col_error}-\ref{fig:feedforward_with_fc_with_col_external_torques}.
As expected, the collision induces large tracking errors (\RefFig{fig:feedforward_with_fc_with_col_error}) while keeping the external torques relatively low, with a maximum of 12 N.m at around 2s. (\RefFig{fig:feedforward_with_fc_with_col_external_torques}).
The behavior is very close to an ideal control with no feedback.
However, the presence of the friction compensation torques (\RefFig{fig:feedforward_with_fc_with_col_torques}) makes the robot slowly converge to the target when the perturbation disappears (\RefFig{fig:feedforward_with_fc_with_col_error}).

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.2\textwidth]{torque_control/joints_kuka.png}
	\caption[Joints numbering of a Kuka LWR4+]{Joints numbering of a Kuka LWR4+\addtocounter{footnote}{-1}\footnotemark}
	\label{fig:joints_kuka}
\end{figure}
\footnotetext{Source \cite{Wolinski:15}.}

In the last experiment (case 4), we introduce the PD controller to enhance the position tracking accuracy using the following gains: $\Bold{K}_p = \Diag(150, 500, 100, 200, 50, 20, 20)$, $\Bold{K}_v = \Diag(5, 5, 3, 3, 2, 1, 1)$.
These gains are tuned so that the robot still presents some compliance.
It can be seen from \RefFig{fig:feedforward_with_fc_plus_pd_with_col_error} that the tracking error is improved, even in the presence of a collision. Since the robot really reacts to the collision by generating high torques, the external torques are also higher than previously, as seen in figures \ref{fig:feedforward_with_fc_plus_pd_with_col_torques} and
\ref{fig:feedforward_with_fc_plus_pd_with_col_external_torques}.


These experiments confirm that special care is required when dealing with real world static friction and that the proposed approach allows for an effective compensation without enforcing any perturbation rejection behavior, i.e. the robot can be as soft or as stiff as desired.
A video of these experiments is joint to the manuscript\footnote{Also avaible at \url{https://youtu.be/fvHh1080I5I}}.


\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.9\textwidth]{torque_control/feedforward_only_error.eps}
	\caption{Position command $\Command\Joints$ and response $\Joints$, feedforward only (case 1).}
	\label{fig:feedforward_only_error}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_only_torques.eps}
	\caption{Torque commands, feedforward only (case 1).}
	\label{fig:feedforward_only_torques}
\end{figure}


\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_with_fc_torques.eps}
	\caption{Torque commands, feedforward with friction compensation (case 2).}
	\label{fig:feedforward_with_fc_torques}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_with_fc_error.eps}
	\caption{Position command and response, feedforward with friction compensation (case 2).}
	\label{fig:feedforward_with_fc_error}
\end{figure}




\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_with_fc_with_col_error.eps}
	\caption{Position command and response, feedforward with friction compensation and a collision (case 3).}
	\label{fig:feedforward_with_fc_with_col_error}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_with_fc_with_col_torques.eps}
	\caption{Torque commands, feedforward with friction compensation and a collision (case 3).}
	\label{fig:feedforward_with_fc_with_col_torques}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_with_fc_with_col_external_torques.eps}
	\caption{External torques, feedforward with friction compensation and a collision (case 3).}
	\label{fig:feedforward_with_fc_with_col_external_torques}
\end{figure}



\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_with_fc_plus_pd_with_col_error.eps}
	\caption{Position command and response, feedforward with friction compensation plus PD control and a collision (case 4).}
	\label{fig:feedforward_with_fc_plus_pd_with_col_error}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_with_fc_plus_pd_with_col_torques.eps}
	\caption{Torque commands, feedforward with friction compensation plus PD control and a collision (case 4).}
	\label{fig:feedforward_with_fc_plus_pd_with_col_torques}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\textwidth]{torque_control/feedforward_with_fc_plus_pd_with_col_external_torques.eps}
	\caption{External torques, feedforward with friction compensation plus PD control and a collision (case 4).}
	\label{fig:feedforward_with_fc_plus_pd_with_col_external_torques}
\end{figure}

\section{Conclusion}

In this chapter, we discussed about the importance of having a dynamic model including static friction to provide external forces and torques estimation and to enable low level torque control.
It was noticed that static friction cannot be compensated using a Coulomb friction model, since it is not uniform across the joint space.
Instead, we proposed the use of an output-limited PD stiction controller to quickly compensate deviations from the trajectory.
This compensator was incorporated into a computed torque scheme to enable precise joint positioning.
Experiments showed that a good stiction compensation strategy is required otherwise the joints stays still when subject to low acceleration profiles.
The proposed solutions was proven effective to quickly compensate for the stiction related errors without enforcing a stiff joint positioning.

In the next chapter, we will expose a framework for safe human-robot physical collaboration.
In this framework, the control output is the vector of target joint positions that can be used as input to \RefEq{eq:computed_torque_control_friction_torque_PD}.
The internal position controller of the Kuka LWR4+ is very sensitive to the input and could not be used to achieve the fast reactive motions often required during pHRI.
Instead, the use of the proposed controller allowed to realize any motion within the mechanical limits of the robot.
