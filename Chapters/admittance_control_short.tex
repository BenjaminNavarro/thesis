\chapter{Task space control solutions}


\section{INTRODUCTION}
In recent years, a growth of interest for physical human robot interaction (pHRI) has emerged in robotics research~\cite{Al:06},~\cite{HaAlHi:09},~\cite{LuFl:12}. The goal is to enable collaborative working between human and robots, since ~\Italic{Cobots} (Collaborative Robots) can improve the flexibility of industrial processes, while decreasing the operators' fatigue~\cite{Kruger:09}.

In such scenarios, the robot must adopt a safe behaviour to minimize the risk of injuries to close coworkers. However, until recently, precise requirements for a collaborative robot were not specified. In 2011, in the last revision of the ISO10218 standard~\cite{ISO10218}, the International Organization for Standardization included requirements for a safe industrial robot. This standard specifies that any robot must respect velocity, power and contact force limits at the tool control point (TCP) in the presence of a human. These three limits are fixed by the end-user, depending on the performed task and on the degree of collaboration between operator and robot.

To our knowledge, present-day collaborative robot manufacturers~\cite{Ro:15} fulfill the standard by saturating the velocity, stopping the robot, or by using expensive hardware solutions. Although novel safe actuation systems have been recently proposed in the literature~\cite{SaRoZi:04},~\cite{Al:04},~\cite{BiGrSc:08}, these are not always easily affordable or adaptable for any robotic system. 

An alternative comes from control, typically impedance control~\cite{hogan1985impedance}, and its modified versions for force tracking~\cite{Jung:04}, force limitation \cite{almeida1999force}, adaptive damping~\cite{Duchaine:09} or exploiting redundancy~\cite{Sadeghian2014experimental}. However, to the best of our knowledge, the only work that explicitly tackles the ISO10218-2011 from a control viewpoint is~\cite{Vick:13}. Nevertheless, this controller only takes into account the force limitation imposed by the standard. 

In the frame of the SISCob project, and as an extension of the authors' previous work \cite{Navarro:16}, we propose a full admittance controller (with stiffness, damping and mass parameters) that automatically adapts to the external wrench to guarantee that all three ISO10218 limitations (force, velocity, and power) are respected. The controller is also capable of force control while keeping the safety constrains satisfied. It has been validated on a robotic hand-arm system in the context of a collaborative screwing task.

The paper is organized as follows. In Section II, we define the main variables and general concepts. Sections III and IV respectively describe the adaptive hybrid force-admittance controller and the complete framework that we design to respect force, velocity, and power limitations. The experimental validation on a robotic hand-arm system in Section~V. In this section, we also explain the use of tactile sensing, both to regulate the grasp and to provide an intuitive HRI interface. We summarize and conclude in Section VI.


\section{Definitions}

We consider a robot with $j$ degrees of freedom (dof) and one control point (CP), and denote: $\Joints \in \RealNumbers^j$ its joint values, $\BaseFrame{\Pose} \in \mathbb{SE}(3)$ the CP pose, and $\BaseFrame{\Twist} = \Hvec{\Trans{\Linvels} ~ \Trans{\Angvels} }$ the CP kinematic screw, both expressed in the robot base frame. Velocities can be mapped between the CP frame and the base frame using:
\begin{align}
\BaseFrame{\Twist} &= ~\SpatialTransform{B}{T}\ToolFrame{\Twist} \\
\ToolFrame{\Twist} &= ~\SpatialTransform{T}{B}\BaseFrame{\Twist} = ~^B\Trans{\Bold{V}}_T \BaseFrame{\Twist}, \\
\end{align}
with $\SpatialTransform{B}{T}$ being the spatial motion transform matrix defined by:
\begin{align}
\SpatialTransform{B}{T} &= \begin{bmatrix}
\RotationMatrix{B}{T} & \NullMatrix{3} \\
\NullMatrix{3} & \RotationMatrix{B}{T},
\end{bmatrix}
\end{align}
where $\RotationMatrix{B}{T}$ is the rotation matrix between the CP and the base frame. The mapping between joint space and task space velocities is achieved using the Jacobian matrix $\Jacobian \in \RealNumbers^{6 \times j}$:
\begin{align}
\BaseFrame{\Twist} &= \Jacobian \JointsVel. \label{eq:jacobian}.
\end{align}
To cope with the general case where $j \neq 6$, the inverse mapping can be realized using $\Pinv{\Jacobian}$, the Moore-Penrose pseudo-inverse of $\Jacobian$:
\begin{align}
\Pinv{\Jacobian} &= \Inv{(\Trans{\Jacobian}\Jacobian)}\Trans{\Jacobian} \\
\JointsVel &= \Pinv{\Jacobian} \BaseFrame{\Twist}
\end{align}

\section{Two-layer safe damping control framework}
The controller proposed in this section is based on damping control, a special case of impedance control \cite{Hogan:85}, that can be described in both Cartesian and joint spaces with:
\begin{align}
\Command\Forces &= \DampingMatrix_c\Delta\Twist, \label{eq:damping_control_cart} \\
\Command\Torques &= \DampingMatrix_j\Delta\JointsVel, \label{eq:damping_control_joint}
\end{align}

where $\DampingMatrix_c \in \RealNumbers^{6\times6}$ and $\DampingMatrix_j \in \RealNumbers^{j\times j}$ are diagonal positive matrices of damping parameters. $\Delta\Twist = \Twist - \Reference\Twist$ and $\Delta\JointsVel = \JointsVel - \Reference\JointsVel$ are the errors between the current and reference velocities. $\Command\Forces$ and $\Command{\Torques}$ are the resulting output forces and torques.
\ref{eq:damping_control_cart} and \ref{eq:damping_control_joint} can be rewritten in order to output velocity commands based on input forces and torques and reference velocities:
\begin{align}
\ToolFrame{\Command\Twist} &= \Inv{\DampingMatrix}_c~\ToolFrame\Forces + \ToolFrame{\Reference\Twist}. \label{eq:damping_control_admittance_cart} \\
\Command\JointsVel &= \Inv{\DampingMatrix}_j~\Torques + \Reference\JointsVel. \label{eq:damping_control_admittance_joint}
\end{align}
While \ref{eq:damping_control_admittance_cart} and \ref{eq:damping_control_admittance_joint} were proven to be useful to comply with interaction forces while following a predefined trajectory, they can be extended to fit many more scenarios. To this end, we build a more generic two-layer controller that includes sets of force inputs $\ForceInputsSet$, velocity inputs $\VelocityInputsSet$ and $\JointVelocityInputsSet$ and torque inputs $\TorqueInputsSet$  :
\begin{align}
\ToolFrame{\Command\Twist} &= \Inv{\DampingMatrix_c}\sum\limits_{i \in \ForceInputsSet}\ToolFrame\Forces_i + \sum\limits_{i \in \VelocityInputsSet}\ToolFrame\Twist_i,  \\
\Command{\JointsVel} &= \Inv{\DampingMatrix_j}\sum\limits_{i \in \TorqueInputsSet}\Torques_i + \sum\limits_{i \in \JointVelocityInputsSet}\JointsVel_i
\end{align}
with $\Abs{\ForceInputsSet} ,\Abs{\VelocityInputsSet} ,\Abs{\TorqueInputsSet}, \Abs{\JointVelocityInputsSet} \in \NaturalNumbers$. In this framework, the total velocities, forces and torques can be computed with:
\begin{align}
\Total{\JointsVel} &= \Pinv{\Jacobian} \SpatialTransform{B}{T} \ToolFrame{\Command\Twist} + \Command{\JointsVel} \label{eq:damping_control_framework} \\
\Total{\ToolFrame{\Twist}} &= \ToolFrame{\Command\Twist} + \SpatialTransform{T}{B}\Jacobian\Command{\JointsVel} \\
\Total{\Torques} &= \sum\limits_{i \in \TorqueInputsSet}\Torques_i + \Trans{\Jacobian} \sum\limits_{i \in \ForceInputsSet}\ToolFrame\Forces_i \\
\Total{\Forces} &= \TransPinv{\Jacobian}\sum\limits_{i \in \TorqueInputsSet}\Torques_i + \sum\limits_{i \in \ForceInputsSet}\ToolFrame\Forces_i
\end{align}
With this solution, real world forces as well as virtual ones can be mixed and velocity sources other than a reference trajectory can be added. In this work, the focus has been made to interaction forces, virtual mass and stiffness effects, attractive and repulsive forces generated by a field of potential and also to velocities generated by trajectory generators and a force control law. This is of course not restrictive and many other inputs can be considered. \\

When considering safety during human-robot interactions, most of the solutions can be expressed as some form of velocity reduction. This includes stopping the robot upon contact, reducing its velocity when nearby operators are approaching and imposing velocity, kinematic energy or exchanged power limitations. To cope with these issues, we add a scaling factor $\ScalingFactor \in \Interval{0}{1}$ into \ref{eq:damping_control_framework}:
\begin{align}
\JointsVel &= \ScalingFactor ~ \Total{\JointsVel}. \label{eq:safe_damping_control_framework}
\end{align}
In order to compute the appropriate value for $\ScalingFactor$, we define a set $\ConstraintsSet$ of constraints $\ConstraintsSet_i \in \PositiveNumbers$ to satisfy, with $\Abs{\ConstraintsSet} \in \NaturalNumbers$. The value of $\ScalingFactor$ is then set to:
\begin{align}
\alpha = \min(1, \ConstraintsSet_1, \ConstraintsSet_2, \dots, \ConstraintsSet_k). \label{eq:scaling_factor}
\end{align}
Equations \ref{eq:safe_damping_control_framework} and \ref{eq:scaling_factor} give us a generic framework for safe physical human-robot interaction and collaboration. In the next sections, we will detail the different constraints and control inputs studied.

\section{Constraints}

\subsection{Stop} \label{sec:stop_constraint}
\Bold{Need to find a proper name for this one.} \\

A simple way to provide some level of safety, has it has been demonstrated in ..., is to stop the robot's motion when a contact with a nearby operator occurs. This is mainly used in situations where a robot that relies only on proprioception works closely with humans and any physical interaction between the two agents is prohibited. To provide such a mechanism we can define the following constraint:
\begin{align}
\ConstraintsSet_{stop}(t) = \begin{cases}
0 & \text{if } \Abs{\ForcesExt} > F_{th}^H \text{ or } \Abs{\TorquesExt} > \Torque_{th}^H\\
1 & \text{if } \Abs{\ForcesExt} < F_{th}^L \text{ and } \Abs{\TorquesExt} < \Torque_{th}^L\\
\ConstraintsSet_{stop}(t - \SampleTime) & \text{otherwise.} \\
\end{cases} \label{eq:stop_constraint}
\end{align}
In \ref{eq:stop_constraint}, $\ForcesExt$ and $\TorquesExt$ are the external forces and external joint torques applied to the robot, the $H$ and $L$ superscript letters denote the activation and deactivation thresholds respectively and $\SampleTime$ is the controller's sample time. Using \ref{eq:stop_constraint} in \ref{eq:scaling_factor} results in a complete stop of the robot when the external force or torques pass one of the activation thresholds. Motion can be resumed only when both $\Abs\ForcesExt$ and $\Abs{\TorquesExt}$ go below the deactivation thresholds.

\subsection{Velocity limitation} \label{sec:velocity_limitation}
An other very common safety criteria is velocity limitation. This point is often part of safety standards currently applied in robotics, such as the ISO10218-2011 \cite{ISO10218}. Moreover, even with a carefully planed trajectory respecting the imposed velocity limitation, real or virtual forces applied to the robot can lead to an increase in velocity breaking the safety requirements. To deal with this and respect any velocity limitation in any situation, we create the following constraint:
\begin{align}
\VelocityConstraint = \frac{\VelocityLimit}{\Abs{\Total{\Twist}}}, \label{eq:velocity_constraint}
\end{align}
where $\VelocityLimit \in \RealNumbers^+$ is the maximum velocity allowed. By using this formulation, the value of $\VelocityConstraint$ will stay above 1 as long as robot's total velocity is lower than $\VelocityLimit$ and so won't have any affect in \ref{eq:scaling_factor}.

\subsection{Power limitation} \label{sec:power_limitation}
An other safety criteria in the ISO10218-2011 standard is power limitation. Power can be limited at a low level, e.g. electric power as with the Kuka LWR4, or at the control level. When no such limitation is implemented by the robot's hardware, we propose a new constraint that can be added to the controller to limit the amount of exchanged power. Let's first define the following power measurement:
\begin{align}
\Power = \DotP{\ToolFrame\ForcesExt}{\ToolFrame{\Total{\Twist}}}. \label{eq:power}
\end{align}
Then, using \ref{eq:power}, we can define the associated power constraint:
\begin{align}
\PowerConstraint = \begin{cases}
\frac{\PowerLimit}{\Abs\Power} & \text{if } \Power < 0 \\
1 & \text{otherwise, }
\end{cases}
\end{align}
$\PowerLimit \in \RealNumbers^+$ being the maximum exchanged power allowed. It can be noticed that the limitation can only be effective when the power is negative. This way the velocity will only be reduced when the robot represent a potential threat to the operator. This is illustrated in Fig. \ref{fig:safe_unsafe_power}.
\begin{figure}[ht]
	\center
	\includegraphics[width=0.7\textwidth]{admittance_control/safe_unsafe_power.eps}
	\caption{Safe and unsafe power values}
	\label{fig:safe_unsafe_power}
\end{figure}


\subsection{Force limitation} \label{sec:force_limitation}
Not sure I'll include this one. It was flawed in ICRA16 and it's a bit hacky to include the same expected behavior. In the end, true force limitation is impossible IMHO so maybe find a new name for it if it has to be included. 

\subsection{Kinetic energy limitation} \label{sec:kinetic_energy_limitation}
In the case of a robot colliding with a human operator, kinetic energy is closely related to the level of injury endured by the operator. Haddadin et al. have proposed a methodology to find a mapping between the kinetic energy, the impactor's shape and the induced level of injury \cite{Haddadin_2012}. It is clear that kinetic energy is a major concern when it comes to safety and so must be limited. The kinetic energy of a rigid body is defined as:
\begin{align}
\KineticEnergy = \frac{1}{2} \Mass \Abs{\Twist}^2 \label{eq:kinetic_energy}
\end{align}
where $\Mass$ is the mass of the body. Limiting the kinetic energy can be seen as a form of velocity limitation. As such, the following constraint can derived from \ref{eq:velocity_constraint}:
\begin{align}
\KineticEnergyConstraint = \frac{\sqrt{\frac{2\KineticEnergyLimit}{\Mass}}}{\Abs{\Total{\Twist}}}. \label{eq:kinetic_energy_constraint}
\end{align}
Plugging \ref{eq:kinetic_energy_constraint} into \ref{eq:scaling_factor} yields to a total kinetic energy limited to $\KineticEnergyLimit$. 

\subsubsection{The case of manipulators}
When controlling a manipulator it is clear that \ref{eq:kinetic_energy} can't be applied to compute its kinetic energy, but an alternative solution can be found. We first recall the joint space dynamics of a rigid robot:
\begin{align}
\JointsTorques = \MassMatrix\JointsAcc + \CoriolisCentripetal\JointsVel + \GravityVector, \label{eq:rigid_robot_dynamics}
\end{align}
$\Joints \in \RealNumbers^n$ is the vector of joint positions, $\MassMatrix \in \RealNumbers^{n \times n}$ is the robot's inertia matrix, $\CoriolisCentripetal \in \RealNumbers^{n \times n}$ is the Coriolis and centripetal matrix, $\GravityVector \in \RealNumbers^n$ is the vector of gravity torques and $\JointsTorques \in \RealNumbers^n$ the joint torques. As it has been shown in \cite{Khatib:95}, it is possible to define an operational space kinetic energy matrix:
\begin{align}
\KineticEnergyMatrix(\Joints) = \Inv{(\Jacobian_p(\Joints) \Inv{\InertiaMatrix} \Trans{\Jacobian_p}(\Joints))}, \label{eq:kinetic_energy_matrix}
\end{align}
with $\Jacobian_p(\Joints)$ being the Jacobian matrix that relates operational space velocities at the expected collision point\footnote{This can be chosen as the robot's closest point to the closest operator.} $p$ to joint space velocities. This kinetic energy matrix can be decomposed\cite{Haddadin_2012} into:
\begin{align}
\Inv{\KineticEnergyMatrix}(\Joints) = \begin{bmatrix}
\Inv{\KineticEnergyMatrix}_v(\Joints) & \bar{\KineticEnergyMatrix}_{v\omega}(\Joints) \\
\Trans{\bar{\KineticEnergyMatrix}}_{v\omega}(\Joints) & \Inv{\KineticEnergyMatrix}_\omega(\Joints).
\end{bmatrix}
\end{align}
The equivalent mass perceived at the impact point along the direction of the unit vector $\DirectionVector$ pointing toward the closest operator is:
\begin{align}
\Equivalent\Mass = \Inv{(\Trans{\DirectionVector} \Inv{\KineticEnergyMatrix}_v(\Joints) \DirectionVector)}. \label{eq:equivalent_mass}
\end{align}
The equivalent mass computed with \ref{eq:equivalent_mass} can then be used in \ref{eq:kinetic_energy_constraint} to limit the kinetic energy of the manipulator.

\subsection{Separation distance}  \label{sec:separation_distance}
If the separation distance between the robot and the nearby operators is monitored then it can be used to adapt the limits imposed to the robot. Indeed, only a low level of security may be required if no one is present in its surrounding whereas very strict limitations may be imposed when working closely or in collaboration with humans. A simple example is depicted in Fig. \ref{fig:separation distance}.
\begin{figure}[ht]
	\center
	\includegraphics[width=0.5\textwidth]{admittance_control/separation_distance.eps}
	\caption{Velocity limitation depending on separation distance}
	\label{fig:separation distance}
\end{figure}
To accommodate with this, we define an interpolation function that allows a smooth adaptation of the limits depending on the distance $d_{min}$ to the closest operator or any other object of interest:
\begin{align}
f_{sd}(d_{min},\!x^-,\!x^+,\!y^-,\!y^+) = \begin{cases}
y^- & \text{if } d_{min} \leq x^-,\\
y^+ & \text{if } d_{min} \geq x^+,\\
f_p(d_{min},\!x^-,\!x^+,\!y^-,\!y^+) & \text{otherwise.} 
\end{cases} \label{eq:interpolation_function}
\end{align}
Here, $f_p(d_{min}, x^-, x^+, y^-, y^+)$ is a fifth-order polynomial with null first and second derivatives at $x^-$ and $x^+$. A detailed expression of such a function is given in Sec. \ref{sec:poly_traj_gen}. As an example, we can use \ref{eq:interpolation_function} to tune the velocity limit in \ref{eq:velocity_constraint} so that the robot comes at a complete stop when an operator is at less than $50cm$ while still be able to move up to $0.25m.\Inv{s}$ when no one is present in a $2m$ radius:
\begin{align}
\VelocityLimit = f_{sd}(d_{min}, 0.5, 2, 0, 0.25).
\end{align}
Figure \Ref{fig:interpolation_function} represents the evolution of $\VelocityLimit$ using the given parameters.

\begin{figure}[h!]
	\center
	\includegraphics[width=0.8\textwidth]{admittance_control/separation_distance_vlim.eps}
	\caption{Interpolation function $f_{sd}$ for $x^-=0.5$, $x^+=2$, $y^-=0$ and $y^+=0.25$.}
	\label{fig:interpolation_function}
\end{figure}

\section{Force inputs}

\subsection{Interaction forces}  \label{sec:interaction_forces}
In many cases, it is necessary to adapt the robot's motion in the presence of external forces, e.g for kinesthetic guidance or teaching or to move the robot away from an unpredicted collision. In such scenarios, the external force $\ForcesExt$ can be included in $\ForceInputsSet$. If this is the only force input in $\ForceInputsSet$ then the controller is similar to a typical damping controller.

\subsection{Virtual stiffness and mass}  \label{sec:virtual_stiffness_mass}
Using a full admittance model by including stiffness and mass effects in \ref{eq:damping_control} has been intensively investigated in the literature (\Bold{PUT REFERENCES}) and has been proven useful in many cases (\Bold{PUT MORE REFERENCES}). Let's first recall the complete impedance law \cite{Hogan:85}:
\begin{align}
\Forces = \StiffnessMatrix\Delta\Pose + \DampingMatrix\Delta\Twist + \MassMatrix\Delta\Acceleration \label{eq:impedance}
\end{align}
where $\Delta\Pose = \Pose - \Reference{\Pose}$, $\Delta\Acceleration = \Acceleration - \Reference{\Acceleration}$ and $\StiffnessMatrix$ and $\MassMatrix$ are diagonal positive semi-definite matrices of stiffness and mass parameters respectively. \ref{eq:impedance} can be rewritten in:
\begin{align}
\Twist = \Inv{\DampingMatrix}(\Forces - \StiffnessMatrix\Delta\Pose - \MassMatrix\Delta\Acceleration) + \Reference{\Twist}. \label{eq:admittance}
\end{align}
We can then derive the two force inputs needed to emulate \ref{eq:admittance} by the controller:
\begin{align}
\StiffnessForce &= -\StiffnessMatrix\Delta\Pose \label{eq:stiffness_force} \\
\MassForce &= -\MassMatrix\Delta\Acceleration. \label{eq:mass_force}
\end{align}
Using $\ForceInputsSet = \Set{\ForcesExt, \StiffnessForce, \MassForce}$ will result in classical admittance control.

\subsection{Potential field method}  \label{sec:potential_field_method}
If the robot has to avoid collisions with operators or perform a motion in an unmodeled cluttered environment, a collision avoidance algorithm has to be used. Solutions have been proposed in \Bold{PUT REFERENCES}. Here, we demonstrate how already existing obstacle avoidance mechanisms can be implemented in this framework. We will take the case of the potential field method (PFM) \cite{Khatib:86}, where obstacles are described as repulsive forces and the target location as an attractive one. Summing all these forces result in a motion in the most promising direction, i.e the solution is not global and local minima may prevent the robot from reaching is target position. Dealing with this limitation would require a complete knowledge of the environment which is usually not available due to the use of proximity sensors (e.g LIDAR) or cameras. The PFM can be summed up as:
\begin{align}
\AttractiveForce &= K_{att} \boldsymbol\eta_{att}, \label{eq:attractive_force} \\
\RepulsiveForce &= \begin{cases}
\sum\limits_{i} K_{{rep}_i}\left(\frac{1}{d_{0_i}} -  \frac{1}{d_{{rep}_i}} \right) \boldsymbol\eta_{{rep}_i} & \text{if } d_{{rep}_i} < d_0 \\
0 & \text{otherwise}.
\end{cases} \label{eq:repulsive_force}
\end{align}
In \ref{eq:attractive_force} and \ref{eq:repulsive_force},  $K_{att}$ and $K_{rep}$ are positive scalar gains, $\boldsymbol\eta_{att}$ and $\boldsymbol\eta_{{rep}_i}$ are unit vectors pointing from the robot to the target point and the i-th repulsive object respectively. $d_{{rep}_i}$ is the distance to the i-th repulsive object and $d_{0_i}$ the distance at which the repulsive effect appears. In this framework, $\AttractiveForce$ can be omitted if some stiffness effect $\StiffnessForce$ is already included. Since repulsive forces grow to infinity when $d_{rep} \rightarrow 0$, it is recommended to include the velocity constraint $\VelocityConstraint$ into the constraints set $\ConstraintsSet$ to avoid potentially dangerous motions.

\section{Velocity inputs}

\subsection{Trajectory generators}
We will detail three complementary methods used in this work. The first one allows to generate trajectories containing multiple way points under velocity and acceleration constraints with arbitrary initial and final position, velocity and acceleration. The second one permits the generation of trajectories in Cartesian space using unit quaternions to represent the orientations while keeping the translational and rotational velocities and acceleration under a given limit. The last one monitors the tracking error to pause the trajectory generation when it becomes too large and resume it when the robot is close enough of the target pose. It avoids the robot trying to catch up with the trajectory in the case it has been stopped (\ref{sec:stop_constraint}), slowed down (\ref{sec:velocity_limitation}, \ref{sec:power_limitation}, \ref{sec:kinetic_energy_limitation}) or pushed away (\ref{sec:interaction_forces}, \ref{sec:potential_field_method}).

\subsubsection{Polynomial trajectory generation} \label{sec:poly_traj_gen}
The trajectory generator described in this section is based on polynomials and allow the user to specify multiple segments with initial and final positions, velocities and acceleration associated with a maximum velocity and acceleration or a fixed completion time. Synchronization at each way point or at the initial and final points will also be detailed. \\
Each segment of the trajectory is represented by a fifth-order polynomial (\ref{eq:fifth_order_polynomial}) since it offers the six degrees of freedom needed to accommodate with the six initial and final constraints (\ref{eq:poly_pos_constraint}-\ref{eq:poly_acc_constraint}):
\begin{align}
y(t) &= at^5+bt^4+ct^3+dt^2+et+f \label{eq:fifth_order_polynomial} \\
y(0) &= y_i & y(T) &= y_f \label{eq:poly_pos_constraint} \\
\dot{y}(0) &= \dot{y}_i & \dot{y}(T) &= \dot{y}_f \label{eq:poly_vel_constraint} \\
\ddot{y}(0) &= \ddot{y}_i & \ddot{y}(T) &= \ddot{y}_f, \label{eq:poly_acc_constraint}
\end{align}
where $T$ is the duration of the segment. In order to compute the polynomial's coefficients, we put the problem into a matrix form:
\begin{align}
\begin{bmatrix}
y(0) \\ \dot{y}(0) \\ \ddot{y}(0) \\ y(T) \\ \dot{y}(T) \\ \ddot{y}(T)
\end{bmatrix} = 
\begin{bmatrix}
0&0&0&0&0&1 \\
0&0&0&0&1&0 \\
0&0&0&2&0&0 \\
T^5&T^4&T^3&T^2&T&1 \\
5T^4&4T^3&3T^2&2T&1&0 \\
20T^3&12T^2&6T&2&0&0
\end{bmatrix}
\begin{bmatrix}
a \\ b \\ c \\ d \\ e \\ f
\end{bmatrix}
&=
\begin{bmatrix}
y_i \\ \dot{y}_i \\ \ddot{y}_i \\ y_f \\ \dot{y}_f \\ \ddot{y}_f
\end{bmatrix},
\end{align}
that can simply be solved with:
\begin{align}
\PolynomialCoefficients(T, y_i, \dot{y}_i, \ddot{y}_i, y_f, \dot{y}_f, \ddot{y}_f) = \begin{bmatrix}
a \\ b \\ c \\ d \\ e \\ f
\end{bmatrix}
=
\Inv{\begin{bmatrix}
	0&0&0&0&0&1 \\
	0&0&0&0&1&0 \\
	0&0&0&2&0&0 \\
	T^5&T^4&T^3&T^2&T&1 \\
	5T^4&4T^3&3T^2&2T&1&0 \\
	20T^3&12T^2&6T&2&0&0
	\end{bmatrix}}
\begin{bmatrix}
y_i \\ \dot{y}_i \\ \ddot{y}_i \\ y_f \\ \dot{y}_f \\ \ddot{y}_f
\end{bmatrix}. \label{eq:poly_coeffs_solution}
\end{align}
If the segment has to be completed in a given time, one can just use \ref{eq:poly_coeffs_solution} to compute the coefficients of the polynomial. But in the case where the time is unknown but velocity and acceleration limits are given, $T$ is parameter to determine. Let's first consider the specific case of null initial and final velocities and accelerations:
\begin{align}
\PolynomialCoefficients(T, y_i, 0, 0, y_f, 0, 0) = \Trans{\begin{bmatrix}
	\frac{6\Delta y}{T^5} & -\frac{15\Delta y}{T^4} & \frac{10\Delta y}{T^3} & 0 & 0 & 0
\end{bmatrix}},
\end{align}
with $\Delta y = y_f - y_i$. Solving $\ddot{y}(t) = 0$ gives us the time at which the velocity is maximal, which is $t_{v_{max}} = \frac{T}{2}$. The maximum velocity is then determined by:
\begin{align}
\dot{y}_{max}(T) = \frac{30\Delta y}{16T},
\end{align}
which leads to the minimum time required to satisfy the velocity limit $V_{max}$:
\begin{align}
T_{min,v} = \frac{30\Delta y}{V_{max}},
\end{align}
with $V_{max} \in \StrictlyPositiveNumbers$. The same reasoning can be applied to the acceleration limit $A_{max}$ resulting in:
\begin{align}
T_{min,a} = \sqrt{\frac{10\Delta y\sqrt{3}}{3A_{max}}},
\end{align}
with $A_{max} \in \StrictlyPositiveNumbers$. The minimum duration required for a segment to satisfy both constraints is:
\begin{align}
T = \max(T_{min,v}, T_{min,a}).
\end{align}
When the initial and final velocities and accelerations are non zero, no trivial solution can be found for $T$. To solve this problem we can use Alg.\ref{alg:segment_min_time} that, given an initial value for $T$, will converge to the minimum time necessary to comply with both constraints. The first part will solve $T$ for the velocity limit $\dot{y}_{max}$ and the second one for the acceleration constraint $\ddot{y}_{max}$. At the end, the maximum between both time is retained. In this algorithm, the minimum durations updates $(*)$ and $(**)$ for $T_{min,v}$ and $T_{min,a}$ are exact in the specific case described above and are assumed to be a good approximation in the general case. \Bold{PROVIDE SOME BENCHMARKS TO ASSESS THE FAST CONVERGENCE OF THE ALGORITHM.}

\begin{algorithm}
	$T_v = T_a = 1$ \\
	\Repeat{$\Abs{\Delta_v} < \varepsilon_v$}{
		$v_{max} = \Abs{\dot{\Polynomial}_{max}(\PolynomialCoefficients(T, y_i, \dot{y}_i, \ddot{y}_i, y_f, \dot{y}_f, \ddot{y}_f))}$ \\
		$\Delta_v = v_{max} - \dot{y}_{max}$ \\
		$T_{min,v} = T_{min,v} \frac{v_{max}}{\dot{y}_{max}} ~ (*)$
	}
	\Repeat{$\Delta_a < \varepsilon_a$}{
		$a_{max} = \Abs{\ddot{\Polynomial}_{max}(\PolynomialCoefficients(T, y_i, \dot{y}_i, \ddot{y}_i, y_f, \dot{y}_f, \ddot{y}_f))}$ \\
		$\Delta_a = a_{max} - \ddot{y}_{max}$ \\
		$T_{min,a} = T_{min,a} \sqrt{\frac{ a_{max}}{\ddot{y}_{max}}} ~ (**)$
	}
	$T = \max(T_{min,v}, T_{min,a})$
	\caption{Segment minimum time computation} \label{alg:segment_min_time}.
\end{algorithm}

\subsubsection{Constrained trajectory generation}
%Using unit quaternions to describe orientation in three dimensional space has several advantages over Euler angles (simpler composition, no gimbal lock) or rotation matrices (more compact and more stable numerically). Using unit quaternions over Euler angles has the disadvantage that their interpolation under velocity and acceleration (or higher derivatives) constraints is not trivial. To overcome this, we propose a method to convert the quaternion interpolation problem in a form that allows to use them with any interpolation method (polynomial, bang-bang trajectories, etc). We define the orientation quaternion as:
\begin{align}
\Quaternion &= \CreateQuaternion{\Bold{q}_v}{q_w}
\end{align}
with $\Bold{q}_v$ being the vector part and $q_w$ the scalar part. The target orientation is denoted $\Reference{\Quaternion}$. We can compute an angular error vector $\DeltaTheta$ between $\Reference{\Quaternion}$ and $\Quaternion$ vector using:
\begin{align}
\Delta\Quaternion(t) &= \Reference{\Quaternion}(t) \overline{\Quaternion}(0) \\
\phi &= \begin{cases}
2 \Inv{\cos}(\Delta\Quaternion_w) & \text{if } \Delta\Quaternion_w \geq 0\\
2 \Inv{\cos}(\Delta\Quaternion_w)-2\pi & \text{otherwise}
\end{cases} \label{eq:phi_quat}\\
\DeltaTheta(t) &= \phi \frac{\Delta\Quaternion_v}{\Norm{\Delta\Quaternion_v}}
\end{align}
Using \ref{eq:phi_quat} gives $\phi \in \OpenLeftInterval{-\pi}{\pi}$, allowing the rotation to perform to be kept at its minimum (e.g rotation of $-\pi$ instead of $\frac{3\pi}{4}$). The trajectory generator can then be configured to output a trajectory going from $\NullVector{3} = \Hvec{0~0~0}$ to $\DeltaTheta(t)$ under the desired velocity and acceleration constraints $\Angvels_{max}$ and $\dot{\Angvels}_{max}$:
\begin{align}
\Command{\DeltaTheta}(t) &= TG(\NullVector{3}, \DeltaTheta(t), \Angvels_{max}, \dot{\Angvels}_{max}),
\end{align}
$TG$ being the trajectory generator being used. After each interpolation, the orientation quaternion to track can be computed as follow:
\begin{align}
\Quaternion_\Delta(t) &= \CreateQuaternion{\frac{\Command{\DeltaTheta}(t)}{2}}{0} \\
\Command{\Quaternion}(t) &= e^{\Quaternion_\Delta(t)} \Quaternion(0)
\end{align}

\subsubsection{Path following}
Maybe not exactly path following but still a way to adapt the trajectory generation to the robot's actual motion. If it is stopped or slowed down by a constraint, the target point will always stay close (constrained error) avoiding the robot trying to catch up with the trajectory.

\subsection{Force control}
An example on how to add other control laws in the framework.

\section{Software implementation}
Describe the open source library without going into too much details. Mainly to say that it exists.

\section{CONCLUSION}

The focus of this paper is the safety of physical interaction with a hand-arm robotic system. 

The main contribution is the design of a closed-loop adaptive admittance and force controller guaranteeing the ISO10218 standard (force, velocity and power limitations). We discuss some interesting aspects that emerge from our approach, but that should be considered whenever dealing with the standard, without dedicated hardware (as in~\cite{SaRoZi:04},~\cite{Al:04},~\cite{BiGrSc:08}). First, the delay in the system response can cause the controller to infringe the ISO10218 when the force varies suddenly. For this, hardware solutions can come in handy but are not always available. Secondly, in some situations, the robot should (surprisingly!) become active instead of compliant. This is the case, e.g., when the force direction is aligned with a strong pre-planned velocity. These two aspects should be tackled with advanced models of the force evolution (e.g., based on model predictive control), which are non-trivial, particularly in the case of deformable contacts.
