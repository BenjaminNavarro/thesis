\chapter{Conclusion}
In this thesis, we explored physical human-robot interaction and collaboration and how control can bring safety to these scenarios.

We first recalled the importance of a dynamic model for implementing low level torque control, estimating the interaction forces and deriving some interesting safety criteria, such as the reflected inertia.
We showed that, when using a computed torque method to achieve position control, the Coulomb friction model can hardly be used to describe the joint dry friction of real robots, such as the Kuka LWR4+.
The proposed solution is a PD based model-free compensator with a very limited output range, that allows a quick compensation of the errors induced by the dry frictions, without altering the desired external perturbation behavior, and instead allowing accurate non-stiff joint positioning.

Then, we exploited damping control, a special case of admittance control, to build a framework to design collaborative tasks while ensuring safety criteria.
In this framework, collaborative tasks can be described in either the joint or task space, using input velocities and forces/torques.
Safety constraints are introduced through velocity reduction, allowing emergency stops, limitation of the joint and/or tool velocity, acceleration and power.
It is also possible to limit the force and the kinetic energy at the end-effector.
Separation distance monitoring, when available, allows to tune these limitations online, to adopt a safer behavior when a human is close and to relax the limitations when no one is present in the workspace.
As mentioned earlier in this thesis, it seems that no single solution capable of dealing with the four collaborative operations defined by the ISO15066 exists to this day.
To remedy this issue, we developed an open source library, called OpenPHRI, that implements everything presented in this chapter and provides an interface for the V-REP simulation software.
The library was proven to be very efficient in terms of computation and memory footprint, allowing execution at 1kHz or more.

In the last chapter, we extended this control framework to both omnidirectional mobile manipulators and robotic hands.
For mobile manipulators, the goal was to mimic a human-like behavior by leaving the base fixed when the arm can execute the task by its own and moving it only when necessary, to reach a distant object or to avoid self collision for example.
To cope with this and the task space redundancy introduced by the mobile base, we proposed to split the velocity generated at the end-effector between the arm and the base according to a set of constraints.
These constraints are based on the arm's kinematic singularities and on the manipulability index, workspace limitations and orientation deviation.
The default behavior is to move only the arm but, when a constraint is approaching, velocities start to be transferred to the mobile base.
When any of the given constraints are reached, the arm is completely stopped and only the mobile base is used to perform the task.
Also, a generic constraint deactivation strategy has been designed to give the control back to the arm when the operator is moving the robot away from a constraint.
In the second part of this chapter, we looked at the estimation of external forces using tactile sensors mounted on the fingertips of a robotic hand and at how to provide a grasping strategy and contact forces regulation, based on the work presented in chapter \ref{sec:task_space_solutions}.
This work was showcased in two different applications, a collaborative screwing task and EMG-based control for tetraplegic patients.

Even if the solutions provided in this thesis aim for better physical human-robot collaborations, many things need to be done before such interactions can be fully considered for real world applications.
First, hardware must improve while reducing the costs.
Collaborative robots with flexible skin and/or joints are yet to be found in robot manufacturers' catalogs but are a necessary step to make robot intrinsically safer to work or interact with.
Better sensing capabilities would benefit a lot to collaborative applications, with, for example, a tactile skin to sense the location and intensity of a contact force anywhere on the robot or even capacitive sensing to detect the direct proximity of humans without relying on cameras or laser scanners.
All this technology already exists to some extent, but has not reached the market yet, limiting its spread.

Then, considering control, a huge amount of solutions have been proposed to solve specific problems, but we are still missing a common framework to tackle any collaborative task, slowing down the progress in the area. Moreover, the lack of open source software makes the integration of previously published work harder and can quickly become very time consuming.

OpenPHRI is a first attempt to bridge this gap but it is not perfect.
Being solely based on kinematics it can not include dynamics-based constraints, such as torque limitations, or incorporate works from others that rely on a dynamic model and on torque control.
These issues will be investigated for future versions of OpenPHRI, to make the library usable in more cases.

While physical human-robot interactions are still mainly focused on serial manipulators, it is important to consider other robot structures to be able to bring a larger set of innovative solutions to current robotics problems.
We investigated the use of mobile manipulators with an omnidirectional base during physical collaborations but non-omnidirectional bases must also be considered since differential drive is common in currently available robots, thanks to its simplicity and low cost.
For non-omnidirectional bases, non-holonomic constraints have to be accounted for. This will lead eventually to a more complex solution that the one we proposed here.
Regarding robotic hands, a device fully covered with a tactile skin would help to have a better sense of the grasped object and would make tactile signing possible, allowing more detailed intention communication to the robot.
Miniaturization of the motorization would make light\footnote{The Shadow Dexterous Hand weighs around 4.5kg.} and dexterous hand design possible, allowing these to be incorporated in other robots, typically humanoid.

Although improvements on control, software and hardware are required before we can have access to truly collaborative robots, we still believe that this thesis contributed to make robots easier to integrate, when designing collaborative applications and safer to work with.
We also hope that this work will contribute positively to the future of collaborative robots.
