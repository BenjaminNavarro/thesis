\chapter{Background and State of the art} \label{sec:state_of_the_art}

The contribution of this thesis is toward better physical interactions and collaborations between robots and human, while ensuring safety criteria to protect both the operators and the environment.
This can be achieved in several ways, starting with specifically designed actuators or dedicated devices to higher level control schemes.
Some background on physical human-robot interaction (pHRI) and collaboration (pHRC) is given in Sect. \ref{sec:soa_hri}.
In Sect. \ref{sec:soa_compliant_actuators}, we will detail how mechanical design and low level controllers has been used as a first step to enable human-robot interaction.
Next, in Sect. \ref{sec:soa_safe_control}, we will see how higher level control solutions has been tailored for pHRI with robotic manipulators, and how safe operation has been ensured.
Finally, in Sect. \ref{sec:soa_other_robots}, we present works where pHRI has been extended to robotic systems other than serial manipulators.

\section{Human-robot interactions} \label{sec:soa_hri}

Human-robot interaction (HRI) has received increasing attention in recent years from both the academic research and the industry \cite{Al:06, HaAlHi:09, LuFl:12}.
HRI is a very vast domain since interactions between a human and a robot can and will occur in various scenarios, either in homes or at work.
These interactions can be split in two main groups, social and physical interactions.
Some terminology employed in HRI will be given in \ref{sec:soa_terminology}, then a brief overview of social human-robot interactions will be given in subsection \ref{sec:soa_shri} before moving to physical interactions (pHRI) (\ref{sec:soa_phri}) and physical collaborations (pHRC) (\ref{sec:soa_phrc}) which are the two main topics of this thesis.

\subsection{Terminology} \label{sec:soa_terminology}

In this thesis, we adopt the task taxonomy presented in \cite{Jarasse:12}.
Some slight modifications to that taxonomy have been performed here, in order to target precisely the work of this thesis without altering the meanings.
A task performed between two agents (humans and/or robots) can be described in two different ways:
\begin{enumerate}
	\setlength{\itemsep}{1pt}
	\item divisible vs. interactive task,
	\item competitive vs. cooperative task.
\end{enumerate}
A \textit{divisible} task employs multiple agents working without any conflict between them, whereas for an \textit{interactive} task, the work has to be performed jointly and simultaneously.
For example, surface related tasks, such as cleaning, mowing or painting, can be divided among the agents while large object transportation can only be performed in an interactive way.
Figure \ref{fig:pHRI} illustrate \textit{divisible} vs. \textit{interactive} tasks, where the robot on the left operates on its own part and has its own workspace, whereas the robot on the right is interacting with a human operator to handover a part.
On the other hand, \textit{competitive} tasks require the agents to work against each other, while joint work is required to perform a \textit{cooperative} task.
A chess game is an example of a competitive task since both agents pursue their own goal. On the other hand, an object assembly, e.g. one agent holding an object and the other putting parts on it, is a cooperative task.
This thesis addresses interactive and cooperative tasks.

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{pHRI.jpg}
\caption[Difference between divisible and interactive tasks]{Difference between divisible and interactive tasks\addtocounter{footnote}{-1}\footnotemark.}
\label{fig:pHRI}
\end{figure}
\footnotetext{Source: \url{www.robotics.org}}

\subsection{Social interaction} \label{sec:soa_shri}

Social human-robot interactions are essentially built around communication between the agents.
This communication can be performed in several ways, including speech \cite{Stiefelhagen:04, Burger:08}, gestures \cite{Stiefelhagen:04, Sato:07}, gaze \cite{Rich:10} or even emotions through voice and facial analysis \cite{Rabiei:16, Banda:15}.
The exchange is bidirectional, since each agent tries to transfer information or intentions to the other one while getting some feedback.
In order for the robot to communicate properly with a human, researchers have first looked into human-human interactions in order to highlight the key aspects that need to be mimicked \cite{Bruce:02, Fincannona:04}.
This is crucial, since humans tend to expect the robot to have a human-like  behavior and will even attribute it some personality traits depending on its actions \cite{Joosse:13}, which can improve or degrade the interactions.
Social human-robot interaction is a very important key towards better collaborative robots but has not been dealt with in this thesis since the focus has been made on physical interaction.

\subsection{Physical interaction}  \label{sec:soa_phri}

Physical human-robot interactions refer to situations where a physical contact occurs between the two agents.
This contact can be either intentional or undesired from the human perspective.
Undesired contacts generally happen when then human enters the robot's workspace while no presence detection system (e.g. light barriers, floor mat, laser scanner) is used and can lead to severe injuries and ultimately death, as it already happened multiple times with industrial robots \cite{Ssanderson:86}.
Voluntary physical interactions, on the contrary, emerge when the person makes contact with the robot to stop it, to guide it or to teach it a behavior for instance.
These two types of physical interaction require different design or control strategies to ensure the humans' safety, as will be detailed respectively in Sections \ref{sec:soa_compliant_actuators} and \ref{sec:soa_safe_control}.

\subsection{Physical collaboration}  \label{sec:soa_phrc}

Collaboration can be seen as a special case of interaction.
In this thesis, we will refer to physical human-robot collaboration for any task performed jointly by a human and a robot, that is both interactive and cooperative.
Collaborations including multiple robots and/or multiple humans are out of scope here.
pHRC has a great potential in many areas where the robot can enhance the human skills or lower the task's difficulty and the associated health risks \cite{Kruger:09}.
This includes robots used for object transportation, as assistive tools, rehabilitation devices or exoskeletons.
During collaborative tasks, it is still crucial that the robot presents a safe behavior but it is also necessary to be intuitive to use and to ease the task completion.
In \cite{Kim:18}, the robot adapts its configuration during a collaborative load carrying task to decrease static joint torques in the human body to limit human fatigue and the risk of injury.
In \cite{Peternel:16b}, human muscular fatigue is estimated in order to provide a higher assistance level when necessary, by adjusting the control parameters.
Such considerations are very important for assistive robots to be effectively accepted and introduced in the industry, health care centers or at home.

\section{Compliant actuators} \label{sec:soa_compliant_actuators}
 In order to be fast and precise, classic industrial robotic manipulators are designed to be very stiff at both joint and structural levels.
This leads to little impact absorption in case of collision.
Moreover, their shape may present sharp edges and the high inertia of their links may induce high kinetic energy dissipation upon impact, leading to severe injuries \cite{Haddadin:12}.
This is why mechanical design is the first step toward safer robots that can be used for interactive tasks.
This question is already partly solved since the birth of so-called light-weight robots \cite{Hirzinger:08}, that present low inertia, thanks to the use of advanced materials, and round shapes to mitigate the injuries and the damages in case of impact.
However, most of these robots still rely on stiff actuation and require attention on the control part to truly behave safely.
Some compliant joint designs have been proposed \cite{SaRoZi:04, Al:04, BiGrSc:08}.
These include series elastic actuators (SEA), where a passive compliant element (e.g., a spring) is introduced to absorb the high frequency impact forces.
In most designs, the passive element stiffness can be adjusted, to be rigid and precise when moving at low velocities or compliant during high speed motions to limit the injuries or damages induced by an impact~\cite{Bicchi:05}.
A schematic view of these types of actuators is given in \RefFig{fig:actuators}.
The elastic element can also be used to store, then release, energy for more efficient walk or for throwing objects for instance, as demonstrated in \cite{Dean:08} and \cite{Haddadin:09}.
The major drawback of such mechanisms is that they limit the torque control bandwidth, introducing inaccuracies in the controlled position, even in absence of collision.
To deal with this problem, distributed macro-mini actuators (DM²) have been proposed \cite{SaRoZi:04}.
With this solution, the joint actuation is divided between two motors: a powerful one being fixed at the robot base, actuating the joint trough cables and elastic coupling, and another smaller one at the joint level with a stiff, low friction and high bandwidth actuation.
This gives the same properties as SEA, while being able to provide high frequency torques thanks to the joint level actuators, thus increasing accuracy, force control performance and disturbances rejection.
Also, having only small motors at the joint level decreases the overall inertia, making the robot safer to work with.
The main issue here (that limited the proliferation of this solution) is the added complexity and cost to doubling the actuators.
But even with a proper mechanical design, control needs to be taken into account for safe operations, since predictive actions can be performed and mechanical compliancy, when present, has a limited range of action and can only be used as the first security measure before the control can take over.
The goal of the ANR SISCob project, in which our laboratories are involved, is to increase the robots' safety with on one hand, the design of a new modular device that brings safety using intrinsic compliance, in the vein of the SEAs, and on the other hand, to approach safety using control.
Control techniques for pHRI are reviewed in the next section.

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{actuators.jpg}
\caption[Stiff, series elastic and variable stiffness actuators.]{Stiff, series elastic and variable stiffness actuators\setcounter{footnote}{0}\footnotemark.}
\label{fig:actuators}
\end{figure}
\footnotetext{Source: \cite{Furnemont:16}.}


\section{Control for safe physical human-robot interaction and collaboration} \label{sec:soa_safe_control}

During interaction and collaboration with a human, the robot must adopt a safe behavior to minimize the risk of injuries to close coworkers.
However, until recently, precise requirements for a collaborative robot were not specified.
In 2011, in the last revision of the ISO10218 standard~\cite{ISO10218}, the International Organization for Standardization included requirements for a safe industrial robot.
This standard specifies that any robot must respect velocity, power and contact force limits at the tool control point (TCP) in the presence of a human.
In the original standard specification, numerical values were given for these limitations (0.25m.s$^{-1}$, 80W, 150N) but these are now left to be fixed by the end-user, depending on the performed task and on the degree of collaboration between operator and robot.
The ISO/TS 15066~\cite{ISO15066} further extends the ISO10218 by defining four types of collaborative operation, illustrated in \RefFig{fig:iso15066}, that can also be combined:
\begin{itemize}
	\item \textit{Safety-rated monitored stop}: interactions with the robot are only allowed when the robot is stopped. Automatic operation is resumed when the operator leaves the collaborative workspace.
	\item \textit{Hand-guiding operation}: the operator guides the robot using physical contact.
	\item \textit{Speed and separation monitoring}: the robot speed is reduced as the operator gets closer to the robot. A protective stop is issued when a potential contact occurs.
	\item \textit{Power and force limitation}: exerted force and transmitted power are limited to avoid any harm to the operator in the case of accidental contact with the robot. A risk assessment for each body region must be performed to derive the imposed limitations.
\end{itemize}

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{iso15066.jpg}
\caption[Illustration of the ISO15066 standard.]{Illustration of the ISO15066 standard\setcounter{footnote}{0}\footnotemark. a) \textit{Safety-rated monitored stop}, b) \textit{Hand-guiding operation}, c) \textit{Speed and separation monitoring}, d) \textit{Power and force limitation}.}
\label{fig:iso15066}
\end{figure}
\footnotetext{Source \cite{Rosenstrauch:17}.}


The \textit{safety-rated monitored stop} is what most present-day robot manufactures~\cite{Ro:15} provide, since it is very simple to integrate with presence detection systems already available.
Any intrusion detected in the robot workspace will trigger a protective stop.
\textit{Hand-guiding operation} is also proposed by some manufacturers~\cite{Ro:15}, since it allows non-programming experts to perform tasks with a collaborative robot (e.g. heavy object transportation) hence increasing the production line flexibility.
These kind of tasks fall into what is called teaching-by-demonstration.
Teaching-by-demonstration methods allow the robot to learn from a human specific behaviors to be reproduced.
These range from simple pick and place~\cite{Rengerve:11} tasks to both motion and control gain learning~\cite{Peternel:16} making the robot behave ``as humanly'' as possible.
\textit{Speed and separation monitoring}, to the best of our knowledge, is not directly implemented by robot manufacturers but has been investigated in the research literature, e.g., in~\cite{Marvel:17}.
Finally, \textit{power and force limitation} is a much more challenging problem since it requires first a complete risk assertion study to determine which power and force limits to use, and second specific measures to ensure these limitations.
Robot manufacturers generally implement a fixed electrical power limitation (e.g. 80W for the original ISO10218, as with the Kuka LWR4+) but this may be too restrictive since more power may be needed when no operator is present.
Control solutions can help to better monitor the exchanged power and force and dynamically adapt the limitations.
Force limitation methods can be employed for torque control robots, as it has been shown in~\cite{Vick:13}.
Otherwise, a protective stop or a fallback strategy can be triggered when a force threshold is trespassed.

After this overview of the literature in safe pHRI, it can be seen that no single solution capable of dealing with the four collaborative operations defined by the ISO15066 exists.
In Chapter \ref{sec:task_space_solutions}, we will detail how a unique framework can be constructed to bridge this gap.

\section{Interacting with robots other than serial manipulators} \label{sec:soa_other_robots}
Even if most of the physical human-robot interaction research is performed using serial manipulators, other types of robots can be of interest.
In this survey, as a complement of the classical serial manipulators studied in the first part, we consider mobile manipulators, that benefit from the increased workspace offered by their mobile base, and robotic hands, that can handle more complex objects than simple grippers and that can also rely on tact to sense the environment and interact with humans.
To our knowledge of the literature, applications using dual manipulators in pHRI are not numerous and authors generally consider them as two separate arms\cite{Vick:13}.

\subsection{Mobile manipulators}

Mobile manipulators benefit from the dexterity of a standard manipulator with the extended workspace of a mobile platform.
Locomotion can be realized by wheeled, legged or flying bases.
However, mobile manipulators are over-actuated robots that need specific control algorithms to deal with their redundancy.
Several approaches have been proposed to deal with this issue in non-collaborative cases, depending on the type of mobile base that is used. For wheeled bases, differential drive actuation introduces non-holonomic constraints due to the rolling without slipping of the wheels on the ground.
These constraints limit the set of velocities that can be realized by the mobile base, and that need to be integrated in the controller.
This has been addressed with several approaches, e.g., using a path planning strategy \cite{Tanner:03} or producing a complete kinematics model together with a redundancy scheme \cite{DeLuca:06}.
Instead, for mobile bases with steerable wheels, a global kinematics model cannot be used directly, since velocities on the steering axes do not induce velocities on the robotics platform.
This has been investigated in \cite{DeLuca:10}, where the Jacobian null space projection and a global input-output linearization with dynamic feedback have been tested and compared.

Legged robots equipped with an arm generally fall into two categories, biped and quadruped.
Biped robots, usually adopt a humanoid structure.
For these systems, locomotion and manipulation are tightly coupled, since the robot balance needs to be guaranteed.
Generally, for dealing with the system's high redundancy, researchers use optimization with a set of tasks (e.g., base velocity and hand/s pose/s) and constraints (e.g., stability and self collision avoidance).
This strategy has been applied in \cite{Vaillant:16} and in \cite{Agravante:16}, to achieve human-humanoid interaction.
Quadruped robots equipped with a manipulator have been investigated since \cite{Adachi:96}, where the base is modeled as a parallel robot to define the pose of the arm's base frame and inverse kinematics are used to control the whole robot.

Recently, researchers have embedded manipulators on aerial robots\cite{Jimenez-Cano:13,Alvarezmunoz:14}.
In such scenarios, the dynamic effects of the arm motion must be taken into account, along with redundancy, to keep the robot stable.

When considering physical human interaction with a mobile manipulator, only a few works have been published. In~\cite{Kim:14}, a fully omnidirectional wheeled robot is made compliant using force control.
Physical interaction with mobile manipulators has also been studied in \cite{Jia:10}, where force thresholds are used to decide if the base, arm or both have to move, and in \cite{Leboutet:16} where the use of a tactile skin permits full body compliance.

In section~\ref{sec:mobile_comanipulation_framework}, we will introduce a solution exploiting redundancy for mobile manipulators with omnidirectional bases that focuses on the intuitiveness of operation during human-robot collaborations with a mobile manipulator.

\subsection{Robotic hands}
Hands, robotic or human ones, can be used in various ways, including communicating between agents, environment sensing and object grasp/handover.
Human-robot interactions can greatly benefit from such features.
Take an example where a hand-arm system is used to perform an assembly, with some parts being out of reach.
When the robot has to grasp an object outside of its workspace, it can point it to inform an operator that it requires assistance to perform the task, as in~\cite{Chao:14}.
This non-verbal and non-physical interaction is very powerful, since it is easily understandable by anyone, in contrast with voice based communication.
Once the operator picked up the part, he has to hand it over to the robot, leading to a physical interaction between the two agents.
Human-robot handover has been investigated by the research community and several solutions are available \cite{Mainprice:12,Tang:16}.
Tactile sensing can then be useful to successfully grasp the object, but can also be used as a way to enable physical communication, as shown in \RefSec{sec:tactile_sensing}, where a finger's tactile sensor is used to trigger the various tasks required to insert screws in a wood piece.
Handshaking is another example of physical communication implying robotics hands and had been investigated in~\cite{Papageorgiou:15,Jindai:08}.

In section \ref{sec:hand_control}, we will detail how tactile sensing capabilities of a robotic hand can be used to derive the contact force at the fingertips and how it can be used to enable object grasping and tactile communication with a human operator.

From this review of the state-of-the-art in safe physical human-robot interaction and collaboration, we can notice that a lot of work still need to be done on both the hardware and the control sides to obtain truly safe robots.
This thesis focuses on control, first with serial manipulators at both a low level, as discussed in Chapter \ref{sec:torque_control}, and at a higher level, with the unified framework for safe pHRI presented in Chapter \ref{sec:task_space_solutions}.
And finally, in Chapter \ref{sec:other_robots}, extensions of this framework to omnidirectional mobile manipulators and robotic hands will be presented.
