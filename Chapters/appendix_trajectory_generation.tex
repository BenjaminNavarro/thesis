\chapter{Polynomial interpolation and trajectory generation} \label{app:trajectory}

This appendix will present how fifth-order polynomials are used in this thesis for smooth interpolation and trajectory generation under velocity and acceleration constraints.

\origsection{Fith-order polynomials} \label{app:fifth_order_polynomial}

The order of a polynomial used for interpolation or trajectory generation is given by the number of constraints to satisfy. In our case, we need to impose initial and final values as well as their first and second derivatives. This leads to six constraints that can be satisfied by a polynomial composed of six parameters, hence the use of fifth-order polynomials.

In this section we will recall how such functions are described and how their parameters can be computed to satisfy the given constraints. The equations of a fifth-order polynomial and its two first derivatives are given in \RefEq{eq:fifth_order_polynomial}-\RefEq{eq:fifth_order_polynomial_2nd_derivative}:
%\begin{align}
%\Polynomial(t) &= at^5+bt^4+ct^3+dt^2+et+f \label{eq:fifth_order_polynomial} \\
%\dot{\Polynomial}(t) &= 5at^4+4bt^3+3ct^2+2dt+e \label{eq:fifth_order_polynomial_1st_derivative} \\
%\ddot{\Polynomial}(t) &= 20at^3+12bt^2+6ct+2d \label{eq:fifth_order_polynomial_2nd_derivative},
%\end{align}
\begin{align}
\Polynomial(t, \PolynomialCoefficients) &=  [t^5\ t^4\ t^3\ t^2\ t\ 1]~\PolynomialCoefficients \label{eq:fifth_order_polynomial} \\
\dot{\Polynomial}(t, \PolynomialCoefficients) &=  [5t^4\ 4t^3\ 3t^2\ 2t\ 1\ 0]~\PolynomialCoefficients \label{eq:fifth_order_polynomial_1st_derivative} \\
\ddot{\Polynomial}(t, \PolynomialCoefficients) &=  [20t^3\ 12t^2\ 6t\ 2\ 0\ 0]~\PolynomialCoefficients \label{eq:fifth_order_polynomial_2nd_derivative},
\end{align}
where $t \in \RealNumbers$ and $\PolynomialCoefficients = \Trans{[a\ b\ c\ d\ e\ f]} \in \RealNumbers^6$ are the vector of the polynomial coefficients.
Then, we define the initial and final constraints as:
\begin{align}
\Polynomial(0) &= \Polynomial_i & \Polynomial(T) &= \Polynomial_f \label{eq:poly_pos_constraint} \\
\dot{\Polynomial}(0) &= \dot{\Polynomial}_i & \dot{\Polynomial}(T) &= \dot{\Polynomial}_f \label{eq:poly_vel_constraint} \\
\ddot{\Polynomial}(0) &= \ddot{\Polynomial}_i & \ddot{\Polynomial}(T) &= \ddot{\Polynomial}_f, \label{eq:poly_acc_constraint}
\end{align}
with $T \in \StrictlyPositiveNumbers$. Using $t \in \Interval{0}{T}$ instead of the more general form $t \in \Interval{T_1}{T_2}$ allows simpler expressions and solutions as well as faster computations. To compute the polynomial coefficients, we put the problem in matrix form:
\begin{align}
\begin{bmatrix}
\Polynomial_i \\ \dot{\Polynomial}_i \\ \ddot{\Polynomial}_i \\ \Polynomial_f \\ \dot{\Polynomial}_f \\ \ddot{\Polynomial}_f
\end{bmatrix}
=
\Bold{A} \PolynomialCoefficients
=
\begin{bmatrix}
0&0&0&0&0&1 \\
0&0&0&0&1&0 \\
0&0&0&2&0&0 \\
T^5&T^4&T^3&T^2&T&1 \\
5T^4&4T^3&3T^2&2T&1&0 \\
20T^3&12T^2&6T&2&0&0
\end{bmatrix}
\begin{bmatrix}
a \\ b \\ c \\ d \\ e \\ f
\end{bmatrix}
,
\end{align}
that can then be solved with:
\begin{align}
\PolynomialCoefficients(T, \Polynomial_i, \dot{\Polynomial}_i, \ddot{\Polynomial}_i, \Polynomial_f, \dot{\Polynomial}_f, \ddot{\Polynomial}_f) =
\Inv{\Bold{A}}
\begin{bmatrix}
\Polynomial_i \\ \dot{\Polynomial}_i \\ \ddot{\Polynomial}_i \\ \Polynomial_f \\ \dot{\Polynomial}_f \\ \ddot{\Polynomial}_f
\end{bmatrix}. \label{eq:poly_coeffs_solution}
\end{align}
It can be found that the determinant of $\Bold{A} = -4T^9$, leading to the matrix always being invertible since $T$ is strictly positive. Once the coefficients have been computed, the evaluation of the polynomial and its first and second derivatives can be obtained using \RefEq{eq:fifth_order_polynomial}-\RefEq{eq:fifth_order_polynomial_2nd_derivative}.
When generating multiple polynomials at the same time, the output vectors can be obtained using:
\begin{multicols}{3}
	\begin{equation}
	\boldsymbol{\Polynomial}(t, \PolynomialCoefficients_{traj}) = \begin{bmatrix} {\Trans{\PolynomialCoefficients}_{0}}\\ {\Trans{\PolynomialCoefficients}_{1}} \\ \vdots \\ {\Trans{\PolynomialCoefficients}_{p}} \end{bmatrix} \begin{bmatrix} t^5 \\ t^4 \\ \vdots \\ 1 \end{bmatrix}
	\end{equation} \label{eq:multiple_trajectories}\break
	\begin{equation}
	\boldsymbol{\dot{\Polynomial}}(t, \PolynomialCoefficients_{traj}) = \begin{bmatrix} {\Trans{\PolynomialCoefficients}_{0}}\\ {\Trans{\PolynomialCoefficients}_{1}} \\ \vdots \\ {\Trans{\PolynomialCoefficients}_{p}} \end{bmatrix} \begin{bmatrix} 5t^4 \\ 4t^3 \\ \vdots \\ 0 \end{bmatrix}
	\end{equation} \label{eq:multiple_velocity_trajectories}\break
	\begin{equation}
	\boldsymbol{\ddot{\Polynomial}}(t, \PolynomialCoefficients_{traj}) = \begin{bmatrix} {\Trans{\PolynomialCoefficients}_{0}}\\ {\Trans{\PolynomialCoefficients}_{1}} \\ \vdots \\ {\Trans{\PolynomialCoefficients}_{p}} \end{bmatrix} \begin{bmatrix} 20t^3 \\ 12t^2 \\ \vdots \\ 0 \end{bmatrix} \label{eq:multiple_acceleration_trajectories},
	\end{equation}
\end{multicols}
\noindent with $p \in \NaturalNumbers$ the number of polynomials.

Trajectories should often be described using multiple waypoints. To deal with this, we can split the trajectory into segments, each represented by a polynomial. This can be translated to:
\begin{align}
\Polynomial(t, \PolynomialCoefficients_s) &= 	[t^5\ t^4\ t^3\ t^2\ t\ 1]
	\begin{cases}
	\PolynomialCoefficients^0 & \text{if }{0 \leq t \leq T_0 } \\
	\PolynomialCoefficients^1 & \text{if }{T_0 < t \leq T_1 } \\
	\vdots \\
	\PolynomialCoefficients^q & \text{if }{T_{q-1} < t \leq T_q }
	\end{cases},
\end{align}
with $q \in \NaturalNumbers$ the number of segments. To avoid discontinuities in the trajectory, we impose the following condition:
\begin{align}
\dot{\Polynomial}_i^k = \dot{\Polynomial}_f^{k-1} \\
\ddot{\Polynomial}_i^k = \ddot{\Polynomial}_f^{k-1},
\end{align}
$ \forall k \in \Interval{1}{q}$.

\origsection{Interpolation} \label{sec:interpolation_function}

\begin{figure}[b]
	\center
	\includegraphics[width=0.8\textwidth]{appendix/interpolation_function.eps}
	\caption{Interpolation function $f_{int}$ for $x^-=0.5$, $x^+=2$, $y^-=0$ and $y^+=0.25$.}
	\label{fig:interpolation_function}
\end{figure}

To perform smooth interpolations, we define the following function based on the polynomial described in \RefSec{app:fifth_order_polynomial}:
\begin{align}
\InterpolationFunction(x,x^-,x^+,y^-,y^+) = \begin{cases}
y^- & \text{if } x \leq x^-,\\
y^+ & \text{if } x \geq x^+,\\
\Polynomial(t, \PolynomialCoefficients(T, y^-, 0, 0, y^+, 0, 0)) & \text{otherwise.}
\end{cases} \label{eq:interpolation_function}
\end{align}
with $t = x - x^-$ and $T = x^+ - x^-$. Imposing $\dot{\Polynomial}_i = \ddot{\Polynomial}_i = \dot{\Polynomial}_f = \ddot{\Polynomial}_f = 0$ gives us a smooth function for all real value $x$, as depicted in \RefFig{fig:interpolation_function}, where the evolution of $\InterpolationFunction(x,0.5,2,0,0.25)$ for $x \in \Interval{0}{2.5}$ is given. It can be seen that the constraints are respected and that $\InterpolationFunction$ can be used when smooth interpolation is needed.

\origsection{Trajectory generation} \label{sec:poly_traj_gen}

We will detail four complementary methods used in this work to generate task or joint space trajectories.

The first method, presented in \RefSec{sec:constrained_trajectory_generation}, allows to generate polynomial-based trajectories under velocity and acceleration constraints with arbitrary initial and final position, velocity and acceleration. Similar solutions, such as the Reflexxes Motion Library \cite{Kroger:11}, are already available, with the main differences being bang-bang acceleration profiles, no notion of waypoints to build complex trajectories and that they are only usable with rotations described by Euler angles. Bang-bang acceleration profiles will produce the shortest trajectories between two points, but are very demanding on the robot actuators and can even cause them some damage due to their discontinuous nature. Also, smooth trajectories are preferable during human-robot interaction, since they provide a more natural motion. Using fifth-order polynomials results in smooth and acceleration-continuous trajectories, at the cost of a longer completion time.

The second method allows multiple trajectories, possibly each composed of several segments, to be synchronized. It will be detailed in \RefSec{sec:trajectory_synchronization}.

In \RefSec{sec:quaternion_trajectory}, a method to generate trajectories in task space using unit quaternions while maintaining translational and rotational velocities and accelerations under a given limit will be presented.

The last method monitors the tracking error to pause the trajectory generation when the error becomes too large, and to resume it when the robot is close enough to the target pose. This avoids the robot from ``trying to catch up'' with the trajectory if it has been stopped (Sect.~\ref{sec:stop_constraint}), slowed down (Sections~\ref{sec:velocity_limitation}, \ref{sec:power_limitation}, \ref{sec:kinetic_energy_limitation}) or pushed away (Sections~\ref{sec:interaction_forces}, \ref{sec:potential_field_method}).

%As mentioned previously, the trajectory generator described in this section is based on fifth-order polynomials and allows the user to describe trajectories composed of multiple segments with initial and final positions, velocities and accelerations together with a maximum velocity and acceleration or a fixed completion time. Each trajectory is one dimensional but several of them can be generated at the same time to drive multiple joints or task dimensions and, in that case, synchronization at each waypoint or at the initial and final points can be achieved and will be detailed later.

\subsection{Constrained trajectory generation} \label{sec:constrained_trajectory_generation}
If a trajectory segment has to be completed in a given time, one can just use \RefEq{eq:poly_coeffs_solution} to compute the coefficients of the polynomial. Instead, if the time is not constrained, but velocity and acceleration limits are given, $T$ is a parameter to be determined.

Let us first consider the specific case of null initial and final velocities and accelerations:
\begin{align}
\PolynomialCoefficients(T, \Polynomial_i, 0, 0, \Polynomial_f, 0, 0) = \Trans{\begin{bmatrix}
	\frac{6\Delta \Polynomial}{T^5} & -\frac{15\Delta \Polynomial}{T^4} & \frac{10\Delta \Polynomial}{T^3} & 0 & 0 & 0
	\end{bmatrix}},
\end{align}
with $\Delta \Polynomial = \Polynomial_f - \Polynomial_i$. Solving $\ddot{\Polynomial}(t_{v_{max}}, \PolynomialCoefficients) = 0$ gives us the time at which the velocity is maximal, which is $t_{v_{max}} = \frac{T}{2}$. The maximum velocity is then determined by:
\begin{align}
\max[\dot{\Polynomial}(\PolynomialCoefficients)] = \frac{30\Delta \Polynomial}{16T},
\end{align}
which leads to the minimum time required to satisfy the velocity limit $V_{max}$:
\begin{align}
T_{min,v} = \frac{30\Delta \Polynomial}{V_{max}},
\end{align}
with $V_{max} \in \StrictlyPositiveNumbers$. The same reasoning can be applied to the acceleration limit $A_{max}$ resulting in:
\begin{align}
T_{min,a} = \sqrt{\frac{10\sqrt{3} \Delta \Polynomial}{3A_{max}}},
\end{align}
with $A_{max} \in \StrictlyPositiveNumbers$. The minimum duration required for a segment to satisfy both constraints is:
\begin{align}
T = \max(T_{min,v}, T_{min,a}). \label{eq:trajectory_minimum_time}
\end{align}
When the initial and final velocities and accelerations are non zero, no trivial solution can be found for $T$. To solve this problem we can use Alg.\ref{alg:segment_min_time} that, given an initial value for $T$ (set to 1 here, but could be any $T \in \StrictlyPositiveNumbers$), will converge to the minimum time necessary to comply with both constraints. The first part will solve $T$ for the velocity limit $V_{max}$ and the second one for the acceleration constraint $A_{max}$. At the end, \RefEq{eq:trajectory_minimum_time} is used to obtain the segment duration. In this algorithm, the minimum durations updates $(*)$ and $(**)$ for $T_{min,v}$ and $T_{min,a}$ are exact in the specific case described above and are assumed to be a good approximation in the general case. The maximum velocity $\max[\dot{\Polynomial}(\PolynomialCoefficients)]$ and acceleration $\max[\ddot{\Polynomial}(\PolynomialCoefficients)]$ for a given polynomial can be found using a zero search algorithm, such as \cite{Jenkins:70}. Benchmarks of Alg.\ref{alg:segment_min_time} are presented in \ref{sec:trajectory_generator_benchmark}.

\begin{algorithm}
	$T_{min,v} = T_{min,a} = 1$ \\
	\Repeat{$\Abs{\Delta_v} < \varepsilon_v$}{
		$v_{max} = \Abs{\max[\dot{\Polynomial}(\PolynomialCoefficients(T_{min,v}, \Polynomial_i, \dot{\Polynomial}_i, \ddot{\Polynomial}_i, \Polynomial_f, \dot{\Polynomial}_f, \ddot{\Polynomial}_f))]}$ \\
		$\Delta_v = v_{max} - V_{max}$ \\
		$T_{min,v} = T_{min,v}~ \frac{v_{max}}{V_{max}} ~ (*)$
	}
	\Repeat{$\Delta_a < \varepsilon_a$}{
		$a_{max} = \Abs{\max[\ddot{\Polynomial}(\PolynomialCoefficients(T_{min,a}, \Polynomial_i, \dot{\Polynomial}_i, \ddot{\Polynomial}_i, \Polynomial_f, \dot{\Polynomial}_f, \ddot{\Polynomial}_f))]}$ \\
		$\Delta_a = a_{max} - A_{max}$ \\
		$T_{min,a} = T_{min,a}~ \sqrt{\frac{ a_{max}}{A_{max}}} ~ (**)$
	}
	$T = \max(T_{min,v}, T_{min,a})$.
	\caption{Segment minimum time computation.} \label{alg:segment_min_time}
\end{algorithm}


\subsection{Synchronization} \label{sec:trajectory_synchronization}

When multiple trajectories must be generated simultaneously, some synchronization mechanisms should be used. This is illustrated in \RefFig{fig:trajectory_sync_comp}, where two trajectories $\Polynomial_0$ and $\Polynomial_1$, each composed of two segments, are represented using no synchronization (top), waypoint synchronization (middle) and trajectory synchronization (bottom).
\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{admittance_control/traj_sync/images/trajectory_sync_comp.eps}
	\caption{Comparison of the synchronization mechanisms.}
	\label{fig:trajectory_sync_comp}
\end{figure}
For the the waypoint synchronization, the duration of the shortest segments (the ones of $\Polynomial_0$ in the example) is increased to match with the longest one ($\Polynomial_1$ segments here). For the trajectory synchronization, each segment duration of the shortest trajectories ($\Polynomial_0$) is increased so that their total duration matches the longest one ($\Polynomial_1$). This can be translated to:
\begin{align}
T_j^k &= \begin{cases}
T_{min,j}^k & \text{if no synchronization} \\
\max(T^k) & \text{for waypoint synchronization}\\
T_{min,j}^k + \frac{\max(T_j) - T_{min,j}}{N} & \text{for trajectory synchronization},
\end{cases}
\end{align}
where $T_j^k$ denotes the duration of the k-th segment of the j-th trajectory,  $T_{min,j}^k$ is the minimum desired duration for the segment, $T_{min,j} = \sum_{k=1}^{N} T_{min,j}^k$ the minimum trajectory duration,  $\max(T^k)$ the longest of the k-th segments, $\max(T_j)$ the longest of the trajectories and $N \in \NaturalNumbers$ is the number of waypoints. When $T_{min,j}^k$ is computed to respect velocity and acceleration constraints (i.e., output by Alg.\ref{alg:segment_min_time}), increasing it for synchronization purposes does not violate the initial limits since the segment maximum velocity and acceleration can only be lowered.

\subsection{The case of orientations} \label{sec:quaternion_trajectory}
Using unit quaternions to describe orientation in three dimensional space has several advantages over both Euler angles (simpler composition, no gimbal lock) and rotation matrices (more compact and numerically stable). Nevertheless, using unit quaternions over Euler angles has the disadvantage that their interpolation under velocity and acceleration (or higher derivative) constraints is not trivial. To overcome this, we propose a method to convert the quaternion interpolation problem in a form that allows the trajectory generator described above to be used. We define the orientation quaternion as:
\begin{align}
\Quaternion &= \CreateQuaternion{\Bold{q}_v}{q_w}
\end{align}
with $\Bold{q}_v$ being the vector part and $q_w$ the scalar part. The target orientation is denoted by $\Reference{\Quaternion}$. Then, we can compute an angular error vector $\DeltaTheta$ between $\Reference{\Quaternion}$ and $\Quaternion$ vector using:
\begin{align}
\Delta\Quaternion(t) &= \Reference{\Quaternion}(t) \overline{\Quaternion}(0) \\
\phi &= \begin{cases}
2 \Inv{\cos}(\Delta q_w) & \text{if } \Delta q_w \geq 0\\
2 \Inv{\cos}(\Delta q_w)-2\pi & \text{otherwise}
\end{cases} \label{eq:phi_quat}\\
\DeltaTheta(t) &= \phi \frac{\Delta\Quaternion_v(t)}{\Norm{\Delta\Quaternion_v(t)}}.
\end{align}
Using \RefEq{eq:phi_quat} gives $\phi \in \OpenLeftInterval{-\pi}{\pi}$, allowing the rotation to be kept at its minimum (e.g., a rotation of $-\pi$ instead of $\frac{3\pi}{4}$). The trajectory generator described earlier can then be configured to output a trajectory going from $\NullVector{3} = \Hvec{0~0~0}$ to $\DeltaTheta(t)$ under the desired velocity and acceleration constraints $\Angvels_{max}$ and $\dot{\Angvels}_{max}$:
\begin{align}
\Command{\DeltaTheta}(t) &= \boldsymbol{\Polynomial}(t, \PolynomialCoefficients') \\
\Command{\Angvels}(t) &= \boldsymbol{\dot{\Polynomial}}(t, \PolynomialCoefficients') \\
\Command{\dot{\Angvels}}(t) &= \boldsymbol{\ddot{\Polynomial}}(t, \PolynomialCoefficients').
\end{align}
For pose tracking, the orientation quaternion $\Command{\Quaternion}$ to be tracked can be computed after each interpolation as follow:
\begin{align}
\Quaternion_\Delta(t) &= \CreateQuaternion{\frac{\Command{\DeltaTheta}(t)}{2}}{0} \\
\Command{\Quaternion}(t) &= e^{\Quaternion_\Delta(t)} \Quaternion(0)
\end{align}

\subsection{Path tracking}
When the robot is paused or slowed down due to $\alpha$ in \RefEq{eq:scaling_factor} being lower than 1 while following a trajectory, the tracking error will grow, resulting in abrupt motions when it becomes unconstrained. In fact, the robot will try to reach the current desired position using the shortest path, which may be different from the initially planned one. To avoid such problems, a path following approach can be used. The goal here is to monitor the tracking error and to stop the trajectory generation when the error becomes too large. This translates to:
\begin{align}
t_{TG}(0) &= 0 \\
t_{TG}(t) &= \begin{cases}
t_{TG}(t-\SampleTime) + \SampleTime &\text{if } \Norm{\Reference{\Pose} - \Pose} < \varepsilon_{TG} \\
t_{TG}(t-\SampleTime) &\text{otherwise}
\end{cases} \\
\Command{\Pose} &= \Polynomial(\PolynomialCoefficients', t_{TG}(t)).
\end{align}
Here, we can see that the time given to the trajectory generator is increased only when the tracking error stays below the threshold $\varepsilon_{TG}$. When the error becomes too large, $t_{TG}$ remains constant so that the target pose will no longer be updated. The generation of the trajectory will be resumed only when the last computed target pose $\Command{\Pose}$ is reached within $\varepsilon_{TG}$.


\subsection{Benchmarks} \label{sec:trajectory_generator_benchmark}
Here, we only assess the good performance of algorithm \ref{alg:segment_min_time}. Indeed, the actual evaluation of the polynomial is very fast since it only requires a limited number of additions and multiplications and thus does not requires benchmarking. We defined three trajectories (one for each task space translation), each composed of six segments using three different waypoints. Details of the trajectories are presented in tables \ref{tab:trajectories_waypoints} and \ref{tab:trajectories}. It can be seen that the trajectory on the x axis always has null velocity and acceleration. Thus, its waypoints fall in the specific case described in \ref{sec:constrained_trajectory_generation}. The $y$ axis trajectory is a bit more complicated since the velocity and acceleration at the second waypoint are non-null. Finally, the $z$ axis trajectory is the most complicated one, since non-null velocity and acceleration are imposed to all the waypoints.

All the benchmarks have been run on a computer equipped with an Intel i7-6700HQ @ 2.6GHz running Linux 4.11. Figure \ref{fig:trajectory_param_computation} gives the trajectory parameters computation time using Alg. \ref{alg:segment_min_time} for each individual trajectory and for the three together, using different synchronization methods. The velocity and acceleration thresholds are set to $\varepsilon_v=10^{-6} \text{ m/s}$ and $\varepsilon_a=10^{-6} \text{ m/s}^2$. Figure \ref{fig:trajectory_param_computation} shows that the synchronization method has no noticeable impact on the computation time. Moreover, the average computation time in the worse case scenario (generating $x$, $y$ and $z$ trajectories) is around $40\mu s$, which is suitable even for online use. Table \ref{tab:trajectories_computation_iterations} gives the number of iterations needed to converge to the solution for $\varepsilon_{v,a} = 10^{-6}$ (high precision) and $\varepsilon_{v,a} = 10^{-3}$ (lower precision). It can be seen that for the x axis trajectory, the number of iterations per segment is 4, which is minimal since two iterations are needed to compute both $T_{min,v}$ and $T_{min,a}$ (one to update the minimum duration and a second to check that the solution has been found). Reducing the precision has a great impact on the convergence in non-ideal cases (y and z trajectories) since the number of iterations is approximatively divided by two in this example. \\

\begin{table}[ht]
	\centering
	\caption{Trajectories' waypoints.}
	\begin{tabular}{|c|ccc|ccc|ccc|}
		\toprule
		Axis &  \multicolumn{3}{c|}{P1} & \multicolumn{3}{c|}{P2}  & \multicolumn{3}{c|}{P3}\\
		\midrule
		{}   	& $\Polynomial$	& $\dot{\Polynomial}$	& $\ddot{\Polynomial}$	& $\Polynomial$	& $\dot{\Polynomial}$	& $\ddot{\Polynomial}$	& $\Polynomial$	& $\dot{\Polynomial}$	& $\ddot{\Polynomial}$\\
		\midrule
		x   	& 0 			& 0   					& 0  					& 0.1 			& 0 					& 0 					& 0.3 			& 0 					& 0\\
		y   	& 0 			& 0   					& 0  					& 0.2 			& 0.1 					& 0.05 					& 0.3 			& 0 					& 0\\
		z   	& 0 			& 0.1  					& 0.1  					& 0.2 			& 0.05 					& -0.05					& 0.1 			& -0.1 					& 0.1\\
		\bottomrule
	\end{tabular}
	\label{tab:trajectories_waypoints}
\end{table}
\begin{table}[ht]
	\centering
	\caption{Trajectories.}
	\begin{tabular}{|c|cc|cc|cc|cc|cc|cc|}
		\toprule
		Axis &  \multicolumn{2}{c|}{P1$\rightarrow$P2} & \multicolumn{2}{c|}{P2$\rightarrow$P3}  & \multicolumn{2}{c|}{P3$\rightarrow$P1}  & \multicolumn{2}{c|}{P1$\rightarrow$P3}   & \multicolumn{2}{c|}{P3 $\rightarrow$ P2}   & \multicolumn{2}{c|}{P2$\rightarrow$P1}\\
		\midrule
		{}   	& $\dot{\Polynomial}_{max}$ & $\ddot{\Polynomial}_{max}$ & $\dot{\Polynomial}_{max}$ & $\ddot{\Polynomial}_{max}$ & $\dot{\Polynomial}_{max}$ & $\ddot{\Polynomial}_{max}$ & $\dot{\Polynomial}_{max}$ & $\ddot{\Polynomial}_{max}$ & $\dot{\Polynomial}_{max}$ & $\ddot{\Polynomial}_{max}$ & $\dot{\Polynomial}_{max}$ & $\ddot{\Polynomial}_{max}$ \\
		\midrule
		x   	& 0.05 & 0.01 & 0.1 & 0.02 & 0.05 & 0.01 & 0.1 & 0.02 & 0.05 & 0.01 & 0.05 & 0.01\\
		y   	& 0.15 & 0.1 & 0.25 & 0.2 & 0.15 & 0.1 & 0.25 & 0.2 & 0.15 & 0.2 & 0.25 & 0.2\\
		z   	& 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2\\
		\bottomrule
	\end{tabular}
	\label{tab:trajectories}
\end{table}
\begin{table}[ht]
	\centering
	\caption{Number of iterations to compute the trajectories.}
	\begin{tabular}{|c|c|c|c|}
		\toprule
		{} &  x & y  & z\\
		\midrule
		Total iterations ($\varepsilon_{v,a} = 10^{-6}$)   		& 24 & 87 & 136\\
		Total iterations ($\varepsilon_{v,a} = 10^{-3}$) 		& 24 & 47 & 63\\
		Iterations per segment (average, $\varepsilon_{v,a} = 10^{-6}$)  & 4 & 14.5 & 22.6\\
		Iterations per segment (average, $\varepsilon_{v,a} = 10^{-3}$)  & 4 & 7.8 & 10.5\\
		\bottomrule
	\end{tabular}
	\label{tab:trajectories_computation_iterations}
\end{table}

\begin{figure}[b]
	\centering
	\includegraphics[width=\textwidth]{admittance_control/benchmarks/images/trajectory_param_computation.eps}
	\caption{Trajectories' coefficients computation benchmark.}
	\label{fig:trajectory_param_computation}
\end{figure}
