\chapter{Introduction}

\begin{figure}[!b]
\center
\includegraphics[width=\textwidth]{robots.png}
\caption[Lightweight robots]{Examples of lightweight robots, from left to right: Kuka LBR iiwa (2013), Rethink Robotics Sawyer (2015), ABB YuMi (2015), Pal Robotics TIAGo (2015), Kawada Industries HRP-4 (2011).}
\label{fig:lightweight_robots}
\end{figure}

The need for collaborative robots (cobots) is becoming more and more important over the years.
The industry seeks cobots that partially automate current manual tasks, to help human workers during difficult tasks by reducing pain, fatigue and the associated risk of injury.
Factories also want to increase their flexibility by allowing non-robotics experts to program new behaviors through learning by demonstration, hence allowing skilled workers to transfer their knowledge to robots.
Health care institutions could also benefit from collaborative robot technology for surgery, physiotherapy and domestic assistance of elderly or disabled people, to mention a few.

Despite the high demand for cobots, there is one aspect that is limiting their proliferation: \textit{safety}.
Indeed, cobots must be capable of ensuring the safety of their operators, other human beings in their surroundings and lastly their own, before being suited for a general adoption.
Safety first comes from mechanical design, starting with the so-called lightweight robots.
These robots present round shapes and low link inertia to lower their kinetic energy, reducing injuries upon impact.
Many robot manufacturers provide such robots in different configurations: single or dual arms and with or without a mobile base, as shown in \RefFig{fig:lightweight_robots}.
Fixed single arm robots are closer to common industrial ones and thus easier to integrate in today's factories.
They also generally present basic safety measures (e.g. collision detection, velocity and power limitation), to simplify the integration regarding current safety standards.
However, since these robots are meant to collaborate with humans, more human-like shapes can be beneficial.
Fixed-base dual-arm robots adopt a human upper-body structure and can act as a human helper for part assembly or inspection.
While fixed-base robots can already be found in some places, mobile manipulators and humanoids are still absent from the factory floor.
Both types benefit from their increased mobility (using either wheels or legs), e.g., to help with object transportation or large product inspections.
On one hand, wheeled cobots, with their inherent stability and the strong research community working on localization, path planning and control for decades, are almost ready to be utilized in factories.
On the other hand, they are limited to relatively flat surfaces and cannot accommodate to any obstacle (e.g. stairs, ladders).
Humanoid robots, by adopting a complete human-like structure (legs, torso, arms, head) are intended to deal with any environment accessible to humans.
However, they are still research products only, since many challenges need to be overcome before their adoption (e.g. locomotion on uneven terrain, stability, fall recovery, mechanical limitations).

When considering collaborative applications, lightweight robots clearly represent an improvement over the classical robotic manipulators currently in use in factories, but they can be enhanced using passive compliance to better absorb shocks due to unexpected collisions.
Passive compliance can take different forms, from soft covers to elastic joint transmissions.
The former can easily be added to existing robots with a relatively low cost, but provides only a limited range of action, while the latter, by deforming the whole structure, has a higher range of action but requires a specific and costly joint design and cannot be added to already existing robots.
In any case, the purpose of mechanical compliance is to serve as a fast impact absorption mechanism before the robot's controller takes over.
Indeed, control can help in various ways to increase the system's safety.
First, preventive actions (typically protective stops or collision avoidance) can be taken to avoid dangerous situations.
Then, if a physical contact occurs, additional compliance can be introduced to overcome the mechanical compliance limits.
This is often performed using impedance or admittance control or one of their many variations.
However, during collaborative tasks, physical contacts between the robot and a human are often necessary.
In that case, different safety measures must be taken, such as velocity, power or force monitoring.
Moreover, the robot must remain stable once the contact with the human is established.
For example, during manual guidance, nothing forbids the operator to move the robot to a singular configuration, an action which will lead to instability if not properly accounted for.

This thesis focuses on the control of collaborative robots and solutions to enforce safe behavior, particularly in the presence of humans.

Figure \ref{fig:general_bloc_diagram} gives an overview of the work discussed in this thesis and its repartition among the chapters.
In this figure, two switches are present, $S_{MM}$ and $S_{HA}$, both represented in their \textit{off} state.
The first one allows to switch between controlling a single robot (\textit{off}) and controlling a mobile manipulator (\textit{on}), using a special redundancy solution.
The second one switches between the control of an arm using torque commands (\textit{off}) and a hand via position commands (\textit{on}).
Since it does not make sense to use the hand along with a mobile base, the case $S_{MM} = S_{HA}$ = \textit{on} is forbidden.

Some background on human-robot interaction and collaboration will be given in Chapter \ref{sec:state_of_the_art}.
Then, in Chapter \ref{sec:torque_control}, we investigate how torque control and external forces estimation can be performed on real robots in the presence of non-modelisable static frictions.
Chapter \ref{sec:task_space_solutions} presents a generic framework to design collaborative tasks using a serial manipulator while ensuring various safety criteria.
Finally, extensions from the classic serial manipulator to mobile manipulators and robotic hands are presented in Chapter \ref{sec:other_robots}.

This thesis has been supported by ANR (French National Research Agency) SISCob (Safety Intelligent Sensor for Cobots) project\footnote{ANR-14-CE27-0016: \url{http://anr-siscob.prd.fr}}.
This project aimed at increasing the safety of collaborative robots by improving their compliance, either though mechanical design with a novel passive compliant device or through control, as discussed in this thesis.

The main contributions of this work are:
\begin{itemize}
  \item A procedure for an active calibration of tactile sensors mounted on a robotic hand, presented in \cite{Navarro:15} and in Section \ref{sec:tactile_sensing}.
  \item A safe adaptive damping control framework answering the constraints of the ISO10218-2011 standard. The results were published in \cite{Navarro:16}.
  \item A framework for the collaboration between a human operator and a mobile manipulator, with a strong emphasis on the intuitiveness of operation. The work has been published in \cite{Navarro:17} and is presented in Section \ref{sec:mobile_comanipulation_framework}.
  \item A generalization of the safe adaptive damping framework, able to deal with many collaborative scenario and freely distributed within the OpenPHRI software. It has been submitted to Robotics and Automation Magazine for the special issue on Human-robot collaboration for production environments. It is presented in Chapter \ref{sec:task_space_solutions}. % TODO update after the acceptance notification (1st of November)
  \item The control of a robotic hand by individuals with tetraplegia using an EMG interface. The study has been published in \cite{Tigra:16} and presented in \ref{sec:hand_test_cases}.
  \item The control in torque of a robot in presence of non-modelisable static frictions, presented in Chapter \ref{sec:torque_control}.
  \item A polynomial-based trajectory generator with velocity and acceleration constraints, suitable for joint or task space trajectories, presented in Appendix \ref{app:trajectory}.
\end{itemize}

\begin{figure}
\center
\includegraphics[width=\textwidth]{schema_bloc.eps}
\caption{General block Diagram of the contents of the thesis and its structure.}
\label{fig:general_bloc_diagram}
\end{figure}
