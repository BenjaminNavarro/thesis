import numpy as np
import matplotlib.pyplot as plt

def setLineStyle(lines, styles):
    for i in range(0, len(lines)):
        lines[i].set_linestyle(styles[i])

def setLineWidth(lines, width):
    for i in range(0, len(lines)):
        lines[i].set_linewidth(width)

 # Simplified coefficient for xi = 0 and dx = xf-xi
 # a = -(12*yi - 12*yf + 6*dx*dyf + 6*dx*dyi - d2yf*dx^2 + d2yi*dx^2)/(2*dx^5)
 # b = (30*yi - 30*yf + 14*dx*dyf + 16*dx*dyi - 2*d2yf*dx^2 + 3*d2yi*dx^2)/(2*dx^4)
 # c = -(20*yi - 20*yf + 8*dx*dyf + 12*dx*dyi - d2yf*dx^2 + 3*d2yi*dx^2)/(2*dx^3)
 # d = d2yi/2
 # e = dyi
 # f = yi
def polynomialCoeffs(xi, xf, yi, yf, dyi, dyf, d2yi, d2yf):
    dx = xf-xi
    dx_2 = dx*dx
    dx_3 = dx_2*dx
    dx_4 = dx_3*dx
    dx_5 = dx_4*dx
    coeffs = np.zeros([10,1])

    coeffs[0] =  -(12*yi - 12*yf + 6*dx*dyf + 6*dx*dyi - d2yf*dx_2 + d2yi*dx_2)/(2*dx_5)
    coeffs[1] =  (30*yi - 30*yf + 14*dx*dyf + 16*dx*dyi - 2*d2yf*dx_2 + 3*d2yi*dx_2)/(2*dx_4)
    coeffs[2] =  -(20*yi - 20*yf + 8*dx*dyf + 12*dx*dyi - d2yf*dx_2 + 3*d2yi*dx_2)/(2*dx_3)
    coeffs[3] =  d2yi/2
    coeffs[4] =  dyi
    coeffs[5] =  yi

    coeffs[6] = xi
    coeffs[7] = xf
    coeffs[8] = yi
    coeffs[9] = yf

    return coeffs

def computePolynomial(x, coeffs):
    if(x < coeffs[6]):
    	y = coeffs[8]
    elif (x > coeffs[7]):
    	y = coeffs[9]
    else:
        x -= coeffs[6]
        x_2 = x*x
        x_3 = x_2*x
        x_4 = x_3*x
        x_5 = x_4*x
        y = coeffs[0] * x_5 + coeffs[1] * x_4 + coeffs[2] * x_3 + coeffs[3] * x_2 + coeffs[4] * x + coeffs[5]
    return y

plt.figure(100, figsize=(8,3))
xi = 0.5
xf = 2
yi = 0
yf = 0.25
step = 0.01
x_vec = np.zeros([int(((xf+2)-(xi-2))/step)+1, 1])
y_vec = np.zeros([int(((xf+2)-(xi-2))/step)+1, 1])
x = xi-2
idx = 0
coeffs = polynomialCoeffs(xi, xf, yi, yf, 0, 0, 0, 0)
while(x < xf+2):
    x_vec[idx] = x
    y_vec[idx] = computePolynomial(x, coeffs)
    idx+=1
    x+=0.01
setLineWidth(plt.plot([xi, xi], [-0.1,1.1], '--'), 2)
setLineWidth(plt.plot([xf, xf], [-0.1,1.1], '--'), 2)
setLineWidth(plt.plot([-0.5, 12.5], [yi, yi], '--'), 2)
setLineWidth(plt.plot([-0.5, 12.5], [yf, yf], '--'), 2)
setLineWidth(plt.plot(x_vec, y_vec), 2)
axes = plt.gca()
axes.set_xlim([0, 2.5])
axes.set_ylim([-0.05, 0.3])
plt.xlabel(r'$d_{min}$')
plt.ylabel(r'$V_{max}$')
plt.legend([r'$x^-$', r'$x^+$', r'$y^-$', r'$y^+$', r'$f$'])
plt.grid()
plt.tight_layout()
plt.savefig('separation_distance_vlim.eps')
#plt.title(r'Polynomial')
plt.show(block=False)

plt.figure(101, figsize=(8,3))
xi = 0.5
xf = 2
yi = 0
yf = 0.25
step = 0.01
x_vec = np.zeros([int(((xf+2)-(xi-2))/step)+1, 1])
y_vec = np.zeros([int(((xf+2)-(xi-2))/step)+1, 1])
x = xi-2
idx = 0
coeffs = polynomialCoeffs(xi, xf, yi, yf, 0, 0, 0, 0)
while(x < xf+2):
    x_vec[idx] = x
    y_vec[idx] = computePolynomial(x, coeffs)
    idx+=1
    x+=0.01
setLineWidth(plt.plot([xi, xi], [-0.1,1.1], '--'), 2)
setLineWidth(plt.plot([xf, xf], [-0.1,1.1], '--'), 2)
setLineWidth(plt.plot([-0.5, 12.5], [yi, yi], '--'), 2)
setLineWidth(plt.plot([-0.5, 12.5], [yf, yf], '--'), 2)
setLineWidth(plt.plot(x_vec, y_vec), 2)
axes = plt.gca()
axes.set_xlim([0, 2.5])
axes.set_ylim([-0.05, 0.3])
plt.xlabel(r'$x$')
plt.ylabel(r'$f_{int}$')
plt.legend([r'$x^-$', r'$x^+$', r'$y^-$', r'$y^+$', r'$f_{int}$'])
plt.grid()
plt.tight_layout()
plt.savefig('interpolation_function.eps')
#plt.title(r'Polynomial')
plt.show()
