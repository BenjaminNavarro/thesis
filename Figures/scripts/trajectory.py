from pylab import *
from minieigen import *
from pyrscl import *

SAMPLE_TIME = 0.010

x_point_1 = NewTrajectoryPoint(-0.197,  0.,     0.)
x_point_2 = NewTrajectoryPoint(-0.25,    0.04,    0.05)
x_point_3 = NewTrajectoryPoint(-0.15,    0.,     0.)

z_point_1 = NewTrajectoryPoint(1.1249,  0.,     0.)
z_point_2 = NewTrajectoryPoint(1.,      -0.025, 0.)
z_point_3 = NewTrajectoryPoint(0.85,    0.,     0.)

x_traj_pos = NewTrajectory(TrajectoryOutputType.Position, x_point_1, SAMPLE_TIME)
z_traj_pos = NewTrajectory(TrajectoryOutputType.Position, z_point_1, SAMPLE_TIME)
x_traj_vel = NewTrajectory(TrajectoryOutputType.Velocity, x_point_1, SAMPLE_TIME)
z_traj_vel = NewTrajectory(TrajectoryOutputType.Velocity, z_point_1, SAMPLE_TIME)
x_traj_acc = NewTrajectory(TrajectoryOutputType.Acceleration, x_point_1, SAMPLE_TIME)
z_traj_acc = NewTrajectory(TrajectoryOutputType.Acceleration, z_point_1, SAMPLE_TIME)

px = x_traj_pos.getOutput()
pz = z_traj_pos.getOutput()
vx = x_traj_vel.getOutput()
vz = z_traj_vel.getOutput()
ax = x_traj_acc.getOutput()
az = z_traj_acc.getOutput()

def addPoints(trajx, trajz):
    trajx.addPathTo(x_point_2, 0.05, 0.1)
    trajx.addPathTo(x_point_3, 0.1, 0.2)
    trajx.addPathTo(x_point_1, 0.05, 0.1)

    trajz.addPathTo(z_point_2, 0.05, 0.1)
    trajz.addPathTo(z_point_3, 0.05, 0.2)
    trajz.addPathTo(z_point_1, 0.05, 0.1)

addPoints(x_traj_pos, z_traj_pos)
addPoints(x_traj_vel, z_traj_vel)
addPoints(x_traj_acc, z_traj_acc)

# Use these for fixed-time paths
# z_traj.addPathTo(z_point_2, 5.)
# z_traj.addPathTo(z_point_3, 5.)
# z_traj.addPathTo(z_point_1, 10.)

trajectory_generator = NewTrajectoryGenerator(TrajectorySynchronization.SynchronizeWaypoints)
trajectory_generator.add("x_traj_pos", x_traj_pos)
trajectory_generator.add("z_traj_pos", z_traj_pos)
trajectory_generator.add("x_traj_vel", x_traj_vel)
trajectory_generator.add("z_traj_vel", z_traj_vel)
trajectory_generator.add("x_traj_acc", x_traj_acc)
trajectory_generator.add("z_traj_acc", z_traj_acc)

trajectory_generator.computeParameters()

waypoints = []
for i in range(x_traj_pos.getSegmentCount()):
    waypoints.append(x_traj_pos.getPathDuration(i))

px_out = []
pz_out = []
vx_out = []
vz_out = []
ax_out = []
az_out = []
times = []
t = 0

end = False
while not (end):
    end = trajectory_generator.compute()
    px_out.append(px.get())
    pz_out.append(pz.get())
    vx_out.append(vx.get())
    vz_out.append(vz.get())
    ax_out.append(ax.get())
    az_out.append(az.get())
    times.append(t)
    t += SAMPLE_TIME

f = figure(1)
subplot(3,2,1)
plot(times, px_out)
plot(waypoints, [])
subplot(3,2,2)
plot(times, pz_out)
subplot(3,2,3)
plot(times, vx_out)
subplot(3,2,4)
plot(times, vz_out)
subplot(3,2,5)
plot(times, ax_out)
subplot(3,2,6)
plot(times, az_out)
show()

print(waypoints)
