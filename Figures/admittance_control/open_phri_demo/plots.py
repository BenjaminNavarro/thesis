import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams.update({'font.size': 20})
plt_idx = 1

def getDataNorm(data):
    shape = data.shape
    data_norm = np.empty((shape[0], 2))
    for idx in range(shape[0]):
        data_norm[idx, :] = [data[idx, 0], np.linalg.norm(data[idx, 1:])]
    return data_norm

def plotData(name, data, ylabel, legend, title, legend_loc='right', line_styles=()):
    global plt_idx
    fig = plt.figure(plt_idx, figsize=(14,4))
    ax = plt.subplot(111)
    plt_idx += 1
    if isinstance(data, tuple):
        curr = 0
        for dat in data:
            style = '-'
            if len(line_styles) > curr:
                style = line_styles[curr]
            curr += 1
            ax.plot(dat[:,0], dat[:,1:], style, linewidth=3)
    else:
        ax.plot(data[:,0], data[:,1:], linewidth=3)
    ax.tick_params(labelsize=20)
    ax.grid()

    if len(legend)>0:
        if legend_loc=='right':
            ax.legend(legend, loc='center left', bbox_to_anchor=(1, 0.5))
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, 0.95, box.height])
        elif legend_loc == 'top':
            ax.legend(legend, loc='upper center', bbox_to_anchor=(0.5, 1.2), ncol=len(legend))
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width, box.height])

    plt.xlabel('Time (s)',fontsize=20)
    plt.ylabel(ylabel, fontsize=20)
    # plt.tight_layout()
    plt.savefig('images/'+name+'.eps', bbox_inches='tight')
    plt.title(title)
    plt.show(block=False)

def plotFile(name, ylabel, legend, title, legend_loc='right', line_styles=()):
    data = np.genfromtxt('log_'+name+'.txt', delimiter="\t", skip_header=0)
    plotData(name, data, ylabel, legend, title, legend_loc, line_styles)

control_point_velocity_norm = getDataNorm(np.genfromtxt('log_controlPointVelocity.txt', delimiter="\t", skip_header=0))

controlPointTotalVelocity = np.genfromtxt('log_controlPointTotalVelocity.txt', delimiter="\t", skip_header=0)[:,0:3]
controlPointVelocity = np.genfromtxt('log_controlPointVelocity.txt', delimiter="\t", skip_header=0)[:,0:3]
t_replay = 35.8
time = controlPointVelocity[:,0]
vlim = np.zeros((time.size, 2))
vlim[:,0] = time
for t in range(0, time.size):
    vlim[t,1] = 0.1 if time[t] < t_replay else 0.15

# plotFile(
#     'controlPointExternalForce',
#     'Forces (N, Nm)',
#     [r'$f_{ext,x}$', r'$f_{ext,y}$', r'$f_{ext,z}$', r'$\tau_{ext,x}$', r'$\tau_{ext,y}$', r'$\tau_{ext,z}$'],
#     'External forces',
#     'top')
# plotFile(
#     'controlPointVelocity',
#      r'Velocity (m.s$^{-1}$, rad.s$^{-1}$)',
#      [r'$V_x$', r'$V_y$', r'$V_z$', r'$\omega_x$', r'$\omega_y$', r'$\omega_z$'],
#      'Control point velocity',
#      'top')
# plotFile(
#     'scalingFactor',
#     r'$\alpha$',
#     [],
#     'Scaling factor')
# plotFile(
#     'controlPointTotalVelocity',
#     r'Velocity (m.s$^{-1}$, rad.s$^{-1}$)',
#     [r'$V_x$', r'$V_y$', r'$V_z$', r'$\omega_x$', r'$\omega_y$', r'$\omega_z$'],
#     'Control point total velocity',
#     'top')
plotData(
    'controlPointVelocityNorm',
    (getDataNorm(controlPointTotalVelocity), getDataNorm(controlPointVelocity), vlim),
    r'Velocity (m.s$^{-1}$)',
    [r'$\Vert \textbf{v}_{tot} \Vert$', r'$\Vert \textbf{v}_{con} \Vert$', r'$V_{max}$'],
    'Velocity',
    'top',
    ('-','-','--'))
# plotFile('traj_vel', r'Velocity (m.s$^{-1}$, rad.s$^{-1}$)', [r'$V_x$', r'$V_y$', r'$V_z$', r'$\omega_x$', r'$\omega_y$', r'$\omega_z$'], 'Control point target velocity')
# plotFile('operator_distance', 'Separation distance (m)', [], 'Separation distance')
# plotFile('sep_dist_vlim', r'Velocity limit (m.s$^{-1}$)', [], 'Velocity limit (sepration distance)')
# plotFile('tavg_fsm_and_controller', r'Computation time ($\mu$s)', [], 'Filtered computation time')

plt.show(block=True)
