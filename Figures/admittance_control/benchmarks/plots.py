import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import msparser

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['text.usetex'] = True

def setLineStyle(lines, styles):
    for i in range(0, len(lines)):
        lines[i].set_linestyle(styles[i])

# Controller timings
if False:
    log_tavg_controller_only = np.genfromtxt('log_tavg_controller_only.txt', delimiter="\t", skip_header=0)
    log_tavg_controller_1cstr = np.genfromtxt('log_tavg_controller_1cstr.txt', delimiter="\t", skip_header=0)
    log_tavg_controller_2cstr = np.genfromtxt('log_tavg_controller_2cstr.txt', delimiter="\t", skip_header=0)
    log_tavg_controller_3cstr = np.genfromtxt('log_tavg_controller_3cstr.txt', delimiter="\t", skip_header=0)
    log_tavg_controller_3cstr_3gen = np.genfromtxt('log_tavg_controller_3cstr_3gen.txt', delimiter="\t", skip_header=0)

    def plotTimings(n, timings, file, title):
        fig = plt.figure(n, figsize=(8,3))
        avg = np.mean(timings)
        std = np.std(timings)
        print("avg:%f, std:%f"%(avg,std))
        plt.plot(timings)
        plt.plot([0, len(timings)], [avg, avg])
        bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
        fig.text(0.95, 0.88, r'$\bar{t} = %.3f\mu s$'%avg+'\n'+r'$\sigma = %.3f\mu s$'%std, ha="right", va="top", size=20,
                bbox=bbox_props)

        plt.tick_params(labelsize=16)
        plt.xlabel('Iteration',fontsize=16)
        plt.ylabel(r'Time ($\mu s$)',fontsize=16)
        plt.legend(['Computation time', 'Average'],fontsize=16, loc='upper left')
        plt.grid()
        plt.tight_layout()
        plt.savefig('images/'+file+'.eps')
        plt.title(title)
        plt.show(block=False)

    plotTimings(1, log_tavg_controller_only[1:,1], 'tavg_controller_only', 'Average time, controller only')
    plotTimings(2, log_tavg_controller_1cstr[1:,1], 'log_tavg_controller_1cstr', 'Average time, controller + velocity constraint')
    plotTimings(3, log_tavg_controller_2cstr[1:,1], 'log_tavg_controller_2cstr', 'Average time, controller + velocity \& power constraints')
    plotTimings(4, log_tavg_controller_3cstr[1:,1], 'log_tavg_controller_3cstr', 'Average time, controller + velocity \& power \& kinetic energy constraints')
    plotTimings(5, log_tavg_controller_3cstr_3gen[1:,1], 'log_tavg_controller_3cstr_3gen', 'Average time, controller + velocity \& power \& kinetic energy constraints + PFM \& stiffness \& force control')

    plt.show(block=True)

# Controller memory usage
if False:
    massif_data = msparser.parse_file('massif.out')

    fig = plt.figure(10, figsize=(8,3))
    heap_memory = []
    stack_memory = []
    total_memory = []
    for snapshot in massif_data['snapshots']:
        heap_memory.append((snapshot['mem_heap'] + snapshot['mem_heap_extra'])/1024)
        stack_memory.append(snapshot['mem_stack']/1024)
        total_memory.append(heap_memory[-1] + stack_memory[-1])
    plt.plot(heap_memory)
    plt.plot(stack_memory)
    plt.plot(total_memory)

    mmax = np.amax(np.array(total_memory))
    idx = np.argmax(np.array(total_memory))
    plt.annotate('%dKiB'%mmax, size=16, xy=(idx, mmax), xytext=(idx+3, mmax + 50), arrowprops=dict(facecolor='black', shrink=0.05))
    plt.ylim(0,300)

    plt.tick_params(labelsize=16)
    plt.xlabel(r'Snapshot',fontsize=16)
    plt.ylabel(r'Memory usage (KiB)',fontsize=16)
    plt.legend(['Heap memory', 'Stack memory', 'Heap + Stack memory'],fontsize=16, loc='upper left', bbox_to_anchor=(0, 1.15))
    plt.grid()
    plt.tight_layout()
    plt.savefig('images/controller_memory_usage.eps')
    plt.title('Heap and stack memory usage')
    plt.show(block=True)

# Trajectory generator timings
if True:
    log_tavg_compute_param_x = np.genfromtxt('log_tavg_compute_param_x.txt', delimiter="\t", skip_header=0, usecols=1)
    log_tavg_compute_param_y = np.genfromtxt('log_tavg_compute_param_y.txt', delimiter="\t", skip_header=0, usecols=1)
    log_tavg_compute_param_z = np.genfromtxt('log_tavg_compute_param_z.txt', delimiter="\t", skip_header=0, usecols=1)
    log_tavg_compute_param_xyz_no_sync = np.genfromtxt('log_tavg_compute_param_xyz_no_sync.txt', delimiter="\t", skip_header=0, usecols=1)
    log_tavg_compute_param_xyz_wp_sync = np.genfromtxt('log_tavg_compute_param_xyz_wp_sync.txt', delimiter="\t", skip_header=0, usecols=1)
    log_tavg_compute_param_xyz_traj_sync = np.genfromtxt('log_tavg_compute_param_xyz_traj_sync.txt', delimiter="\t", skip_header=0, usecols=1)

    groups = (([0], log_tavg_compute_param_x,[0]),
              ([0], log_tavg_compute_param_y,[0]),
              ([0], log_tavg_compute_param_z,[0]),
              (log_tavg_compute_param_xyz_wp_sync, log_tavg_compute_param_xyz_no_sync, log_tavg_compute_param_xyz_traj_sync))

    means_no_sync = []
    means_wp_sync = []
    means_traj_sync = []
    std_no_sync = []
    std_wp_sync = []
    std_traj_sync = []
    for group in groups:
        means_wp_sync.append(np.mean(group[0]))
        std_wp_sync.append(np.std(group[0]))
        means_no_sync.append(np.mean(group[1]))
        std_no_sync.append(np.std(group[1]))
        means_traj_sync.append(np.mean(group[2]))
        std_traj_sync.append(np.std(group[2]))

    fig = plt.figure(20, figsize=(8,3))
    ind = np.arange(4)
    width = 0.2
    plt.bar(ind + width, means_no_sync, width, color='r', yerr=std_no_sync, label='No synchronization')
    plt.bar(ind, means_wp_sync, width, color='y', yerr=std_wp_sync, label='Waypoint synchronization')
    plt.bar(ind + 2*width, means_traj_sync, width, color='b', yerr=std_traj_sync, label='Trajectory synchronization')

    # plt.xlabel('Iteration',fontsize=16)
    plt.tick_params(labelsize=16)
    plt.ylabel(r'Time ($\mu s$)',fontsize=16)
    plt.legend(fontsize=16, loc='upper left')
    plt.xticks(ind + width, ('X', 'Y', 'Z', 'XYZ'))
    plt.grid()
    plt.tight_layout()
    plt.savefig('images/trajectory_param_computation.eps')
    plt.title('Trajectory parameters computation')

    plt.show(block=True)
