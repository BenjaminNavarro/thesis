import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['text.usetex'] = True

log_trajectory_no_sync = np.genfromtxt('log_trajectory_no_sync.txt', delimiter="\t", skip_header=0)
log_trajectory_wp_sync = np.genfromtxt('log_trajectory_wp_sync.txt', delimiter="\t", skip_header=0)
log_trajectory_traj_sync = np.genfromtxt('log_trajectory_traj_sync.txt', delimiter="\t", skip_header=0)
# -------------------------------------
# x_traj: 0., 2.40281, 5.8009
# y_traj: 0., 4.80562, 8.20371
# -------------------------------------
# x_traj: 0., 4.80562, 8.20371
# y_traj: 0., 4.80562, 8.20371
# -------------------------------------
# x_traj: 0., 3.60422, 8.20371
# y_traj: 0., 4.80562, 8.20371

wp_x = (
    ((0., 2.40281, 5.8009),(0., 4.80562, 8.20371)),
    ((0., 4.80562, 8.20371),(0., 4.80562, 8.20371)),
    ((0., 3.60422, 8.20371),(0., 4.80562, 8.20371)),
)

wp_y = ((0, 1, -1),(1, -1, 0))

def subplot(ax, data, title, wp_x, wp_y):
    ax.plot(data[1:,0], data[1:,1:])
    ax.scatter(wp_x[0], wp_y[0], label=r'$\mathcal{P}_0$ waypoints')
    ax.scatter(wp_x[1], wp_y[1], label=r'$\mathcal{P}_1$ waypoints')
    ax.tick_params(labelsize=16)
    ax.grid()
    # ax.set_ylim([-1.2,1.2])
    ax.set_title(title, fontsize=16)
    ax.legend(fontsize=16, loc='upper right', bbox_to_anchor=(1, 1.18))
    # ax3.set_ylabel('Position (m)', fontsize=16)

# fig = plt.figure(1, figsize=(8,3))
f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True, figsize=(12,6))
subplot(ax1, log_trajectory_no_sync, 'No synchronization', wp_x[0], wp_y)
subplot(ax2, log_trajectory_wp_sync, 'Waypoint synchronization', wp_x[1], wp_y)
subplot(ax3, log_trajectory_traj_sync, 'Trajectory synchronization', wp_x[2], wp_y)

ax3.set_xlabel('Time (s)',fontsize=16)
f.subplots_adjust(hspace=0.3)
plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
f.savefig('images/trajectory_sync_comp.eps')
#
# if len(legend)>0:
#     if legend_loc=='right':
#         ax.legend(legend, fontsize=16, loc='center left', bbox_to_anchor=(1, 0.5))
#         box = ax.get_position()
#         ax.set_position([box.x0, box.y0, 0.95, box.height])
#     elif legend_loc == 'top':
#         ax.legend(legend, fontsize=16, loc='upper center', bbox_to_anchor=(0.5, 1.05), ncol=len(legend))
#         box = ax.get_position()
#         ax.set_position([box.x0, box.y0, box.width, box.height])



plt.show(block=True)
