[![build status](https://gitlab.com/BenjaminNavarro/thesis/badges/master/build.svg)](https://gitlab.com/BenjaminNavarro/thesis/builds/artifacts/master/file/thesis.pdf?job=compile_pdf)

# Lastest PDF compiled

The latest build is always available at: https://gitlab.com/BenjaminNavarro/thesis/builds/artifacts/master/file/thesis.pdf?job=compile_pdf

# Typical Git workflow

 * Pull: get the latest version `git pull origin master`
 * Do some work...
 * Add the files to be commited `git add file1 file2` or `git add -A` to add everything
 * Commit the changes: `git commit -m "I added that, I corrected this, ..."`
 * Push: send local commits to the server `git push origin master`
 * A compilation is triggered by the server. The status is indicated at the top of this file. After a push, the status will change to "running" and after a couple of minutes it should goes back to "success". In case of failure, an e-mail will be sent and a new commit fixing the bug should be push asap.
 * 

# Other usefull Git commands
 * status: to see what has changed since the last commit
 * diff: to see what changes have been made to the files since the last commit
